#pragma once
//
//  finalinfo.h
//  movieIndexer
//
//  Created by Brice Rivé on 9/17/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//
#include "movietitle.h"

struct FinalInfo
{
    FinalInfo(const TitleInfo &titleInfo);
    FinalInfo() {}
    string title;
    string originalTitle;
    string year;
    string plot;
    Languages languages;
    vector<string> genres;
    vector<string> directors;
    vector<string> cast;
    string rating;
    string acId;
    string imdbId;
    string poster;
    
    bool HasFR()const { return languages.HasLanguage(Languages::FR); }
    bool HasVO()const { return languages.HasLanguage(Languages::VO); }
    bool HasSTVO()const { return languages.HasLanguage(Languages::STVO); }
    bool HasSTFR()const { return languages.HasLanguage(Languages::STFR); }
    
    /// support boost serialization
    template<class A> void serialize(A &ar, unsigned int) { ar & NVP(title) & NVP(originalTitle) & NVP(year) & NVP(plot) & NVP(languages) & NVP(genres) & NVP(directors) & NVP(cast) & NVP(rating) & NVP(acId) & NVP(imdbId); }
};

