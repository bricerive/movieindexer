//
//  unittest.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "unittest.h"
#include "osxutf8.h"
#include "movietitle.h"
#include "allocine.h"
#include "imdb.h"
#include "strutils.h"
#include "mylib/log.h"
#include "mylib/config.h"
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <vector>

namespace fs=boost::filesystem;
using boost::filesystem::path;
using namespace strutils;
using namespace mylib;
using namespace boost;
using namespace std;

UnitTest::UnitTest()
: workDir("/Users/Brice/Library/Caches/com.bricerive.movieIndexer")
, curl(workDir)
{
    if (!fs::exists(workDir))
        fs::create_directories(workDir);
}

template<typename T>
bool TestValue(const string &item, string &name, const T &expected, const T &found)
{
    if (expected != found)
    {
        LogStream(LOG_TRACE) << item <<" mismatch for ["<<name<<"]: found=["<<found<<"] expected=["<<expected<<"]" << LogStream::Terminator();
        return false;
    }
    return true;
}

void FieldsToVariables(const string &str, vector<string> &vals)
{
    typedef tokenizer<char_separator<char> > tokenizer;
    char_separator<char> sep(":", "", boost::keep_empty_tokens);

    tokenizer tokens(str, sep);
    for (tokenizer::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
        vals.push_back(*tok_iter);
}

void UnitTest::PushTrace(mylib::Log::LogType logLevel)
{
    level = Log::GetLevel();
    Log::SetLevel(logLevel);
}

void UnitTest::PopTrace()
{
    Log::SetLevel(level);
}

bool UnitTest::GuessMovieTitle_ut(const string &line)
{
    // line is fields separated by :
    vector<string> vals;
    FieldsToVariables(line, vals);
    if (!TestValue("Groundtruth", vals[0], 5, static_cast<int>(vals.size()))) return false;
    string gtFile=vals[0];
    string gtGuessedTitle=vals[1];
    string gtGuessedYear=vals[2];
    string gtGuessedLanguage=vals[3];
    string gtGuessedDisambiguator=vals[4];
    
    LogStream(LOG_TRACE) <<"  ["<< gtFile <<"]" << LogStream::Terminator();
    //LogStream(LOG_TRACE) <<"  ["<< gtFile <<"]["<< gtGuessedTitle <<"]["<< gtGuessedYear <<"]["<< gtGuessedLanguage <<"]["<< gtGuessedDisambiguator <<"]" << LogStream::Terminator();

    //  since the movie name is stored in a text file, it has already been encoded back into UTF8
    //  However, we want to test with UTF8_MAC so we reencode here
    //string gtFileUtf8Mac = OsxUtf8::To(gtFile);
    //string movieFileName = OsxUtf8::From(gtFileUtf8Mac);
    string movieFileName = OsxUtf8::From(gtFile);

    //  I'm a little confused here: some gtGuessedTitle from the groundtruth file are in Utf8Mac
    // put them back in utf-8
    gtGuessedTitle = OsxUtf8::From(gtGuessedTitle);
    
    //Trace "    $gtFileUtf8Mac"
    CONFIG_PARAM(Log::LogType, "unitTest", guessLogLevel);
    PushTrace(guessLogLevel);
    TitleInfo titleInfo = MovieTitle::Guess(movieFileName);
    PopTrace();
    
    if (!TestValue("Title", gtFile, gtGuessedTitle, titleInfo.Title()))
    {
        //echo $gtGuessedTitle | hexdump -C
        //echo $title | hexdump -C
        return false;
    }
    if (!TestValue("Year", gtFile, gtGuessedYear, titleInfo.Date()))
    {
        return false;
    }
    if (!TestValue("Language", gtFile, gtGuessedLanguage, titleInfo.LanguageStr()))
    {
        return false;
    }
    if (!TestValue("Disambiguator", gtFile, gtGuessedDisambiguator, titleInfo.Disambiguator()))
    {
        return false;
    }
    //LogStream(LOG_TRACE) <<"  ->["<< gtGuessedTitle <<"]" << LogStream::Terminator();

    return true;
}

bool UnitTest::AllocineGrab_ut(const string &line)
{
    // line is fields separated by :
    vector<string> vals;
    FieldsToVariables(line, vals);
    if (!TestValue("Groundtruth", vals[0], 6, static_cast<int>(vals.size()))) return false;
    string gtGuessedTitle=vals[0];
    string gtGuessedDate=vals[1];
    string gtGuessedLanguage=vals[2];
    string gtTitle=vals[3];
    string gtYear=vals[4];
    string gtId=vals[5];
    LogStream(LOG_TRACE) <<"  ["<< gtGuessedTitle <<"]["<< gtGuessedDate <<"]"<< LogStream::Terminator();
    //LogStream(LOG_TRACE) <<"["<< gtGuessedTitle <<"]["<< gtGuessedDate <<"]["<< gtGuessedLanguage <<"]["<< gtTitle <<"]["<< gtYear <<"]["<< gtId  <<"]"<< LogStream::Terminator();

    TitleInfo titleInfo;
    titleInfo.Title(gtGuessedTitle);
    titleInfo.Date(gtGuessedDate);
    titleInfo.LanguageFromString(gtGuessedLanguage);
    
    // Search allociné
    CONFIG_PARAM(Log::LogType, "unitTest", allocineLogLevel);
    PushTrace(allocineLogLevel);
    AlloCineInfo acInfo;
    int nbHits = AlloCine::GetId(curl, titleInfo, acInfo, workDir+"/allocineSearch.xml");
    PopTrace();

    if (!TestValue("Hits", gtGuessedTitle, 1, nbHits))
    {
        return false;
    }
    if (!TestValue("Id", gtGuessedTitle, gtId, acInfo.id))
    {
        return false;
    }

    PushTrace(allocineLogLevel);
    // query movie info from allocine
    AlloCine::GrabMovieInfo(curl, acInfo, workDir+"/allocineMovie.xml");
    PopTrace();
    
    string escapedTitle = ReplaceString(acInfo.title, ":", "-"); // ':' is the GT delimiter
    if (!TestValue("Title", gtGuessedTitle, gtTitle, escapedTitle))
    {
        return false;
    }
    if (!TestValue("Year", gtGuessedTitle, gtYear, acInfo.year))
    {
        return false;
    }
    LogStream(LOG_TRACE) <<"  -> "<< string(acInfo) << LogStream::Terminator();

    return true;
}

bool UnitTest::ImdbGrab_ut(const string &line)
{
    // line is fields separated by :
    vector<string> vals;
    FieldsToVariables(line, vals);
    if (!TestValue("Groundtruth", vals[0], 6, static_cast<int>(vals.size()))) return false;

    string gtGuessedTitle=vals[0];
    string gtGuessedDate=vals[1];
    string gtGuessedLanguage=vals[2];
    string gtTitle=vals[3];
    string gtYear=vals[4];
    string gtId=vals[5];
    LogStream(LOG_TRACE) <<"  ["<< gtGuessedTitle <<"]["<< gtGuessedDate <<"]"<< LogStream::Terminator();
    //LogStream(LOG_TRACE) <<"["<< gtGuessedTitle <<"]["<< gtGuessedDate <<"]["<< gtGuessedLanguage <<"]["<< gtTitle <<"]["<< gtYear <<"]["<< gtId  <<"]"<< LogStream::Terminator();

    TitleInfo titleInfo;
    titleInfo.Title(gtGuessedTitle);
    titleInfo.Date(gtGuessedDate);
    titleInfo.LanguageFromString(gtGuessedLanguage);

    // Do a search on imdb
    CONFIG_PARAM(Log::LogType, "unitTest", imdbLogLevel);
    PushTrace(imdbLogLevel);
    ImdbInfo imdbInfo;
    int nbImdbHits = Imdb::GetId(curl, titleInfo, imdbInfo, workDir+"/imdbSearch.json");
    PopTrace();

    if (!TestValue("Hits", gtGuessedTitle, 1, nbImdbHits))
    {
        return false;
    }
    if (!TestValue("Id", gtGuessedTitle, gtId, imdbInfo.id))
    {
        return false;
    }

    PushTrace(imdbLogLevel);
    Imdb::GrabMovieInfo(curl, imdbInfo, workDir+"/imdbDetails.json");
    PopTrace();

    string escapedTitle = ReplaceString(imdbInfo.title, ":", "-");  // ':' is the GT delimiter
    if (!TestValue("Title", gtGuessedTitle, gtTitle, escapedTitle))
    {
        return false;
    }
    if (!TestValue("Year", gtGuessedTitle, gtYear, imdbInfo.year))
    {
        return false;
    }
    LogStream(LOG_TRACE) <<"  -> "<< string(imdbInfo) << LogStream::Terminator();
    return true;
}


#define TEST(s) { #s, &UnitTest::s }

bool UnitTest::TestEachLine(const char *testFilesPath, const Test &test)
{
    path ut_file = path(testFilesPath)/(string(test.name)+".txt");
    ifstream listFile(ut_file.c_str());
    
    string line;
    while (getline(listFile, line))
    {
        if (line.empty()) continue;
        if (line=="STOP") break;
        if (line[0]=='#') continue;
        try {
            if (!(this->*test.method)(line)) return false;
        } catch (...) {
            LogStream(LOG_TRACE) << "Test exception on line: " << line 
            << LogStream::Terminator();
        }
        
    }
    return true;
}


bool UnitTest::Process(const char *testFilesPath)
{
    LogStream(LOG_TRACE) << "########################" << LogStream::Terminator();
    LogStream(LOG_TRACE) << "### New Test Session ###" << LogStream::Terminator();
    LogStream(LOG_TRACE) << "########################" << LogStream::Terminator();

    Test tests[] = { TEST(AllocineGrab_ut), TEST(GuessMovieTitle_ut), TEST(ImdbGrab_ut) };
    int nbTests=sizeof(tests)/sizeof(tests[0]);
    //    CONFIG_PARAM(vector<string>, unit_tests);
    
    //  execute the tests
    for (int i=0; i<nbTests; i++)
    {
        LogStream(LOG_TRACE) << "Running test: " << tests[i].name << LogStream::Terminator();

        if (!TestEachLine(testFilesPath, tests[i]))
        {
            LogStream(LOG_TRACE) << "     ---> FAILED" << LogStream::Terminator();
            return false;
        }
        LogStream(LOG_TRACE) << "     ---> PASSED" << LogStream::Terminator();
    }
    LogStream(LOG_TRACE) << nbTests << " tests passed" << LogStream::Terminator();
    return true;    
}


