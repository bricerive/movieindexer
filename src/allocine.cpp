//
//  allocine.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "allocine.h"
#include "levenshtein.h"
#include "cachedcurl.h"
#include "strutils.h"
#include "mylib/log.h"
#include "mylib/idioms.h"
#include "mylib/config.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>

using namespace mylib;
using namespace boost;
using namespace strutils;
namespace pt=boost::property_tree;

//static const char *agent = "Dalvik/1.6.0 (Linux; U; Android 4.2.2; Nexus 4 Build/JDQ39E)";
//static const char *agent = "Dalvik/1.6.0 (Linux; U; Android 4.3.1; Amazon Kindle Fire Build/JLS36I)";
//static const char *agent = "Dalvik/1.2.0 (Linux; U; Android 2.2.2; Huawei U8800-51 Build/HWU8800B635)";
static const char *agent = "Mozilla/5.0 (Linux; U; Android $v; fr-fr; Dell Streak Build/Donut AppleWebKit/5$b.$c+ (KHTML, like Gecko) Version/3.$a.2 Mobile Safari/ 5$b.$c.1";

static const string AlloCineSecretKey = "1a1ed8c1bed24d60ae3472eed1da33eb";

static const char *AllocinePartner="100ED1DA33EB";

int AlloCine::GetId(CachedCurl &curl, const TitleInfo &titleInfo, AlloCineInfo &acInfo, const string &xmlPath)
{
    string url = GetSearchUrl(curl, titleInfo.Title());
    curl.SetAgent(agent);
    curl.GetPayloadFromUrl(url, xmlPath, "www.allocine.fr");
    return AlloCine::GetId(titleInfo, acInfo, xmlPath);
}

struct Dist
{
    bool filterByDate;
    int date;
    int title;
    int otitle;
    Dist(bool filterByDate): filterByDate(filterByDate), date(200), title(32), otitle(32) {}
    bool DateMatch()const {
        CONFIG_PARAM(int, "movieIndexer", allocineDateFuzz);
        return date<=allocineDateFuzz;
    }
    bool ExactDateMatch()const {return date==0;}
    bool ExactTitleMatch()const {return title==0||otitle==0;}
    int TitleDist()const {return min(title, otitle);}
    int DateDist()const {return filterByDate? date: 0;}
    bool TitleMatch()const {return TitleDist()<32;}
    bool operator==(const Dist &rhs)const {return date==rhs.date && title==rhs.title;}
    template<typename T> T&operator<<(T&s)const {s<<date<<" "<<title; return s;}
    int Total()const { return TitleDist() + DateDist(); }
    
    bool operator<(const Dist &d)const
    {
        //   [After.Life.(2009).DVDRIP.VOSTFR.XviD-CREEX]
        // entry[125591|After.Life|After.Life|2009] dist: [0/1/1]
        // entry[21105|After Life|Wandâfuru raifu|1998] dist: [11/0/11]
        //  [Hole In One|2010|VOSTFR|2010]
        // entry[48682|A Hole in one|A Hole in one|2003] dist: [7/2/2]
#if 1
        if (filterByDate && !DateMatch())
            return false;
        return Total() < d.Total();
#else
        if (filterByDate) {
            if (ExactTitleMatch()) {
                if (!d.ExactTitleMatch())
                    return true;
                return DateMatch() && date<d.date;
            }
            if (d.ExactTitleMatch())
                return false;
            if (TitleMatch() && DateMatch()) {
                if (!d.TitleMatch())
                    return true;
                if (date<d.date) {
                    if (TitleDist() <= d.TitleDist())
                        return true;
                    return abs(TitleDist()-d.TitleDist()) < abs(date-d.date);
                }
                if (TitleDist() >= d.TitleDist())
                    return false;
                return abs(TitleDist()-d.TitleDist()) < abs(date-d.date);
            }
            return false;
        }
        return TitleDist()<d.TitleDist();
#endif
    }
};
static std::ostream &operator<<(std::ostream &strm, const Dist &d)
{
    if (d.filterByDate)
        strm <<d.date<<"/";
    strm <<d.title<<"/"<<d.otitle;
    return strm;
} //lint !e1929

// $1 -> title
// $2 -> date
// $3 -> language
// $4 <- id
// Returns:
// number of hits
int AlloCine::GetId(const TitleInfo &titleInfo, AlloCineInfo &acInfo, const string &xmlPath)
{
    LogF(LOG_TRACE,"GetAlloCineId: %s", string(titleInfo).c_str());
    
    
    // load the XML DOM
    pt::ptree pt;
    read_json(xmlPath.c_str(), pt);
    
    bool filterByDate = !titleInfo.Date().empty();
    int date = filterByDate? lexical_cast<int>(titleInfo.Date()): 0;
    string lowercaseTitle = to_lower_copy(titleInfo.Title());
    Dist minDist(filterByDate), startDist(filterByDate);
    string minTitle;
    int nbHits=0;

    pt::ptree *hit;
    vector<pt::ptree *> hits;
    
    BOOST_FOREACH(pt::ptree::value_type &v, pt.get_child("feed"))
    {
        // look at the results count
        if (v.first == "totalResults")
            LogStream(LOG_TRACE) << "  totalResults: " << v.second.get_value<int>() << LogStream::Terminator();
        
        // look at each movie given
        // if we have no hit, can we loosen the date selection a bit?
        // for example, allocine uses production year which is sometimes off by 1 or 2
        // removing the date selection altogether goes against cases where allocine does not know the movie
        // and would give something way off in time that has the same title
        else if (v.first == "movie")
        {

            BOOST_FOREACH(pt::ptree::value_type &p, v.second)
            {
                Dist dist(filterByDate);
                int entryDate = p.second.get("productionYear", 0);
                string entryOTitle = p.second.get<string>("originalTitle","");
                string entryId = p.second.get<string>("code","");
                string entryTitle = p.second.get<string>("title",entryOTitle);
                dist.date = filterByDate? abs(entryDate-date): 0;
                dist.title = Levenshtein::Distance(lowercaseTitle, to_lower_copy(entryTitle));
                dist.otitle = Levenshtein::Distance(lowercaseTitle, to_lower_copy(entryOTitle));
                LogStream(LOG_TRACE) << "  entry["<<entryId<<"|" <<entryTitle<< "|"<<entryOTitle<< "|"<<entryDate<<"] dist: ["<<dist<<"]"<<LogStream::Terminator();
                
                if (dist<minDist)
                {
                    minDist = dist;
                    nbHits = 1;
                    hit = &p.second;
                    minTitle = entryTitle;
                }
                else if (minTitle.empty() && dist==minDist)
                {
                    nbHits++;
                }
            }
        }
    }
    
    if (nbHits==1)
    {
        acInfo.id = hit->get<string>("code");
        acInfo.title = hit->get<string>("title", "");
        acInfo.originalTitle = hit->get<string>("originalTitle", acInfo.title);
        acInfo.year = hit->get<string>("productionYear", "");
    }
    
    LogStream(LOG_TRACE) << "  hits: " << nbHits << LogStream::Terminator();
    LogStream(LOG_TRACE) << "  -> allocineId: " << acInfo.id << LogStream::Terminator();
    return nbHits;
}

static void GetPropertyList(pt::ptree &pt, vector<string> &list)
{
    BOOST_FOREACH(pt::ptree::value_type &l, pt.get_child(""))
    {
        BOOST_FOREACH(pt::ptree::value_type &v, l.second)
        {
            if (v.first=="$")
                list.push_back(v.second.data());
        }
    }
}


// query movie info from allocine
void AlloCine::GrabMovieInfo(CachedCurl &curl, AlloCineInfo &acInfo, const string &xmlPath)
{
    string url = GetMovieUrl(curl, acInfo.id);
    curl.SetAgent(agent);
    curl.GetPayloadFromUrl(url, xmlPath, "www.allocine.fr");
    AlloCine::GrabMovieInfo(acInfo, xmlPath);
}

// $1 -> id
// $2 <- year
// $3 <- poster
// $4 <- rating
// $5 <- genre
// $6 <- plot
// $7 <- title
// $8 <- nationality
// $9 <- titre original
void AlloCine::GrabMovieInfo(AlloCineInfo &acInfo, const string &xmlPath)
{
    LogStream(LOG_TRACE) << "GrabAlloCineMovieInfo: " << acInfo.id << LogStream::Terminator();
    
    // load the XML DOM
    pt::ptree pt;
    read_json(xmlPath.c_str(), pt);
    
    BOOST_FOREACH(pt::ptree::value_type &v, pt.get_child("movie"))
    {
        if (v.first == "nationality")
        {
            GetPropertyList(v.second, acInfo.nationalities);
            LogStream(LOG_TRACE) << "  nationalities: " << to_string(acInfo.nationalities) << LogStream::Terminator();
        }
        else if (v.first == "productionYear")
        {
            acInfo.year = v.second.data();
            LogStream(LOG_TRACE) << "  year: " << acInfo.year << LogStream::Terminator();
        }
        else if (v.first == "poster")
        {
            acInfo.poster = v.second.get<string>("href");
            LogStream(LOG_TRACE) << "  poster: " << acInfo.poster << LogStream::Terminator();
        }
        else if (v.first == "statistics")
        {
            // Double allocine rating so it scales with imdb rating
        string userRating  =v.second.get<string>("userRating", "");
        if (!userRating.empty()) {
            float r = 2 * from_string<float>(userRating);
            std::stringstream streamOut;
            streamOut << std::setprecision(2) << r;
            acInfo.rating = streamOut.str();
            LogStream(LOG_TRACE) << "  rating: " << acInfo.rating << LogStream::Terminator();
        }
        }
        else if (v.first == "genre")
        {
            GetPropertyList(v.second, acInfo.genres);
            LogStream(LOG_TRACE) << "  genres: " << to_string(acInfo.genres) << LogStream::Terminator();
        }
        else if (v.first == "synopsis")
        {
            acInfo.plot = v.second.data();
            LogStream(LOG_TRACE) << "  plot: " << acInfo.plot.substr(0,50) <<"..."<< LogStream::Terminator();
        }
        else if (v.first == "title")
        {
            acInfo.title = v.second.data();
            LogStream(LOG_TRACE) << "  title: " << acInfo.title << LogStream::Terminator();
        }
        else if (v.first == "originalTitle")
        {
            acInfo.originalTitle = v.second.data();
            LogStream(LOG_TRACE) << "  originalTitle: " << acInfo.originalTitle << LogStream::Terminator();
        }
        else if (v.first == "casting")
        {
            BOOST_FOREACH(pt::ptree::value_type &c, v.second)
            {
                string activity = c.second.get<string>("activity", "");
                string name = c.second.get<string>("person.name", "");
                if (activity=="Réalisateur")
                {
                    acInfo.directors.push_back(name);
                }
                if (activity=="Acteur" || activity=="Actrice")
                {
                    if (acInfo.cast.size()<6) acInfo.cast.push_back(name);
                }
            }
            LogStream(LOG_TRACE) << "  directors: " << to_string(acInfo.directors) << LogStream::Terminator();
            LogStream(LOG_TRACE) << "  cast: " << to_string(acInfo.cast) << LogStream::Terminator();
        }
        else if (v.first == "movieType")
        {
        for (pt::ptree::value_type &c: v.second)
        if (c.first=="$")
        acInfo.movieType = c.second.data();
        LogStream(LOG_TRACE) << "  movieType: " << acInfo.movieType << LogStream::Terminator();
        }
    }
}

 #include <openssl/sha.h>
#include "boost/date_time/gregorian/gregorian.hpp"
typedef std::vector<std::pair<const char *, string> > NameValueCollection;
#include "mylib/sha1.h"
static string Today()
{
    using namespace boost::gregorian;
    date today = day_clock::local_day();
    date_facet* facet(new date_facet("%Y%m%d"));
    stringstream os;
    os.imbue(std::locale(std::cout.getloc(), facet));
    os << today;
    return os.str();
}

static std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len)
{
    static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];
    
    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;
            
            for(i = 0; (i <4) ; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }
    
    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';
        
        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;
        
        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];
        
        while((i++ < 3))
            ret += '=';
        
    }
    
    return ret;
    
}


static string GetSignature(const string &query)
{
    CSHA1 csha1;
    return base64_encode(csha1.Encode(query).Code(), 20);
}

/// <summary>
/// Create the Query string including the new URL signature AlloCine API expects
/// </summary>
/// <param name="nvc">The NameValueCollection containing all parameters of the Query string</param>
/// <returns>Returns the search Query string</returns>
static string BuildSearchQueryWithSignature(CachedCurl &curl, const NameValueCollection &nvc)
{
    NameValueCollection collection;
    collection.push_back(make_pair("partner", AllocinePartner));
    
    copy(nvc.begin(), nvc.end(), back_inserter(collection));
    collection.push_back(make_pair("sed", Today()));

    std::stringstream  s;
    BOOST_FOREACH(NameValueCollection::value_type p, collection)
        s << p.first << "=" << p.second << "&";
    string searchQuery = s.str().substr(0, s.str().size()-1);
    
    
    //searchQuery = "partner=100043982026&q=M%C3%A9moires+de+nos+p%C3%A8res&format=json&filter=movie&sed=20140116";
    //searchQuery = "partner=100043982026&q=Mémoires+de+nos+pères&format=json&filter=movie&sed=20140220";
    
    
    
    // sig=iSKXONT9itrF3EZsc2EiTjI0oeg%3D
    //searchQuery = "partner=100043982026&q=M%C3%A9moires+de+nos+p%C3%A8res&format=json&filter=movie&sed=20140220";
    
    
    // sig=46eB5UWbaQhXo5QMeuo26GrmAQM%3D
    //searchQuery = "partner=100043982026&q=Brice+de+Nice&format=json&filter=movie&sed=20140220";
    
    
    //searchQuery = "partner=100043982026&q=La+Notte&format=json&filter=movie&sed=20140220";
    //searchQuery = "partner=100043982026&q=La+Notte&format=json&filter=movie&sed=20140220"; // sig=gB7JlkzFLZjX1pty6Xmg1vlQ%2Flo%3D

    //searchQuery = "partner=100043982026&q=Argo&format=json&filter=movie&sed=20140220"; // &sig=MuQI6zhwFH0p9RO18D%2BKYlbO8LE%3D
    
    //searchQuery = "partner=100043982026&q=Argo&format=json&filter=movie&sed=20140220"; // &sig=MuQI6zhwFH0p9RO18D%2BKYlbO8LE%3D

    string toEncrypt = AlloCineSecretKey + searchQuery;
    
    string signature = GetSignature(toEncrypt);
    string encodedSig = curl.Escape(signature);

    searchQuery += "&sig=" + encodedSig;
    
    return searchQuery;
}

string AlloCine::GetSearchUrl(CachedCurl &curl, const string &title)
{
    NameValueCollection nvc;
    nvc.push_back(make_pair("q", curl.Escape(title)));
    nvc.push_back(make_pair("format", "json"));
    nvc.push_back(make_pair("filter", "movie"));
    nvc.push_back(make_pair("count", "100")); // [Les Enfants](2004) avec Karin Viard et Gerard Lanvin is no 55
    nvc.push_back(make_pair("page", "1"));
    nvc.push_back(make_pair("order", "datedesc"));
        
    // We create the final Query string including the call signature
    return string("http://api.allocine.fr/rest/v3/search?")+BuildSearchQueryWithSignature(curl, nvc);
}

string AlloCine::GetMovieUrl(CachedCurl &curl, const string &id)
{
    NameValueCollection nvc;
    nvc.push_back(make_pair("format", "json"));
    nvc.push_back(make_pair("code", id));
    nvc.push_back(make_pair("profile", "large"));
    nvc.push_back(make_pair("filter", "movie%2Cnews"));
    nvc.push_back(make_pair("striptags", "synopsis"));
    nvc.push_back(make_pair("mediafmt", "mpeg2"));
    
    //We create the final Query string including the call signature
    return string("http://api.allocine.fr/rest/v3/movie?")+BuildSearchQueryWithSignature(curl, nvc);
}

