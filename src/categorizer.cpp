//
//  categorizer.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 9/17/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#include "categorizer.h"
#include "mylib/idioms.h"
#include <boost/foreach.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <fstream>

using namespace fs;
using namespace std;
using namespace boost;
using namespace mylib;

void Categorizer::CategorizeMovies(const string &moviesPath)
{
    if (!exists(moviesPath) || !is_directory(moviesPath)) return;

    directory_iterator end;
    for (directory_iterator itr(moviesPath); itr!=end; ++itr)
    {
        path moviePath(itr->path());
        if (!exists(moviePath) || !is_directory(moviePath))
            continue;
        path infoFile(moviePath/"info.xml");
        if (!exists(infoFile)) continue;
        FinalInfo info;
        LoadInfoFromXml(infoFile, info);
        CategorizeMovie(info, moviePath);
    }
}

void Categorizer::LoadInfoFromXml(const fs::path &xmlPath, FinalInfo &info)
{
    std::ifstream ifs(xmlPath.c_str());
    archive::xml_iarchive oa(ifs);
    oa >> NVP(info);
}
                        

// Categorize the movie:
// If there is a directory named _Classification one or two levels above the movie,
// create soft links to the movie dir in each relevant category folder
void Categorizer::CategorizeMovie(const FinalInfo &finalInfo, const fs::path &movieDir)
{
    path droppedDir=path(movieDir).parent_path();
    static const char *classificationDirName = "_Classification";
    path classificationDirPath = droppedDir/classificationDirName;
    if (!fs::exists(classificationDirPath))
        classificationDirPath = droppedDir.parent_path()/classificationDirName;
    if (!fs::exists(classificationDirPath))
        return;
    
    BOOST_FOREACH(string genre, finalInfo.genres)
    {
        // Traduction des genres imdb en genres allocine
        static struct {const char *imdb, *allocine;} convert[]={
            {"Adventure","Aventure"},
            {"Biography","Biopic"},
            {"Comedy","Comédie"},
            {"Crime","Thriller"},
            {"Documentary","Documentaire"},
            {"Drama","Drame"},
            {"Family","Famille"},
            {"Fantasy","Fantastique"},
            {"Horror","Epouvante-horreur"},
            {"History","Historique"},
            {"Musical","Comédie musicale"},
            {"Mystery","Thriller"},
            {"Short","Court-Metrage"},
            {"Sci-Fi","Science fiction"},
            {"War","Guerre"}
        };
        static const int convertNb=sizeof(convert)/sizeof(convert[0]);
        for (int i=0; i<convertNb; i++) if (genre==convert[i].imdb) {genre=convert[i].allocine; break;}
        AddToCategory(classificationDirPath/"_byGenre"/genre, movieDir);
    }
    
    AddToCategory(classificationDirPath/"_byYear"/finalInfo.year, movieDir);
    
    string rating = to_string(int(from_string<int>(finalInfo.rating)));
    AddToCategory(classificationDirPath/"_byRating"/rating, movieDir);
    
    if (finalInfo.HasFR()) AddToCategory(classificationDirPath/"_byLanguage/FR", movieDir);
    if (finalInfo.HasSTFR()) AddToCategory(classificationDirPath/"_byLanguage/STFR", movieDir);
    if (finalInfo.HasVO()) AddToCategory(classificationDirPath/"_byLanguage/VO", movieDir);
    if (finalInfo.HasSTVO()) AddToCategory(classificationDirPath/"_byLanguage/STVO", movieDir);
    
    BOOST_FOREACH(const string &cast, finalInfo.cast)
    {
        AddToCategory(classificationDirPath/"_byCast"/cast, movieDir);
    }
    BOOST_FOREACH(const string &director, finalInfo.directors)
    {
        AddToCategory(classificationDirPath/"_byDirector"/director, movieDir);
    }
}

#include <unistd.h>

void Categorizer::AddToCategory(const fs::path &categoryDir, const fs::path &movieDir)
{
    if (!exists(categoryDir))
        create_directories(categoryDir);
    path linkDst = categoryDir/movieDir.filename();
    symlink(movieDir.c_str(), linkDst.c_str());
}

