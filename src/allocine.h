#pragma once
//
//  allocine.h
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include "movietitle.h"
#include <string>
#include <vector>
using namespace std;

struct AlloCineInfo
{
    AlloCineInfo(): nbHits(0) {}
    int nbHits;
    bool Found()const {return nbHits==1;}
    string id;
    string year;
    string poster;
    string rating;
    string plot;
    string title;
    string originalTitle;
    vector<string> nationalities;
    vector<string> genres;
    vector<string> directors;
    vector<string> cast;
    string movieType;
    
    operator string()const
    {
        string str = string("[") +title+  "|" +originalTitle+ "|" +year+ "|" +id+ "|" +rating+ "]";
        return str;
    }

};

class CachedCurl;

class AlloCine
{
public:
    static int GetId(CachedCurl &curl, const TitleInfo &titleInfo, AlloCineInfo &acInfo, const string &xmlPath);
    static void GrabMovieInfo(CachedCurl &curl, AlloCineInfo &acInfo, const string &xmlPath);
private:
    typedef std::string string;
    static string GetSearchUrl(CachedCurl &curl, const string &title);
    static string GetMovieUrl(CachedCurl &curl, const string &id);
    static int GetId(const TitleInfo &titleInfo, AlloCineInfo &acInfo, const string &xmlPath);
    static void GrabMovieInfo(AlloCineInfo &acInfo, const string &xmlPath);
};