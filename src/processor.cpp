//
//  processor.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 5/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "processor.h"
#include "categorizer.h"
#include "mylib/log.h"
#include "mylib/idioms.h"
#include "mylib/error.h"
#include <stdlib.h>
#include <stdio.h>
#include "strutils.h"
#include "osxutf8.h"
#include "cachedcurl.h"
#include "levenshtein.h"
#include "movietitle.h"
#include "allocine.h"
#include "imdb.h"
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/archive/xml_oarchive.hpp>

using namespace mylib;
using namespace std;
using namespace strutils;
using namespace boost;
namespace pt=boost::property_tree;
using fs::path;

Processor::Processor()
: workDir("/Users/Brice/Library/Caches/com.bricerive.movieIndexer")
, curl(workDir)
{
    if (!fs::exists(workDir))
        fs::create_directories(workDir);
}

// Process a text file containing a movie directory listing
// $1 the movie list
void Processor::ProcessList(const string &path)
{
    ifstream listFile(path.c_str());
    string moviePath;
    while (getline(listFile, moviePath))
    {
        if (moviePath.empty()) continue;
        if (moviePath=="STOP") break;
        if (moviePath[0]=='#') continue;
        Process(moviePath);
    }
}
// wrapper around Process() to have log redirection and exception handling
void Processor::Process(const string &droppedPath)
{
    // add a log sink to capture the log for this movie
	LogFileSink log((path(workDir)/"log.txt").string());
    
    try {
        ProcessWrapped(droppedPath);
    } catch(std::exception &e) {
        LogF(LOG_ERROR, "Item processing failed: %s", e.what());
    } catch(...) {
        LogF(LOG_ERROR, "Item processing failed");
    }
}


// droppedPath can be different things:
// 1. The path to an existing movie file:
//      "/Users/brice/No Backup/Movies/Les Chèvres du Pentagone-DVDRIP-French.avi"
// 2. The path to an existing movie folder
//      "/Users/brice/No Backup/Movies/John Carter (2012) [VOSTFR]"
// 3. The path to a non-existing movie file (in tests etc.):
//      /Users/brice/MyDocuments/Work/imdbIndexer/test/120526-ClairisPlayerFull/4 Garcons Pleins d'Avenir (1997).avi
// 4. The path to a non-existing movie folder (in tests etc.):
//      "/Users/brice/MyDocuments/Work/imdbIndexer/test/120526-ClairisPlayerFull/(S)ex List [What's Your Number?] (2011) [FR]"
// it is encoded in OsxUtf8
void Processor::ProcessWrapped(const string &droppedPath_)
{
    string droppedPath(droppedPath_);
    droppedPath.erase (remove(droppedPath.begin(), droppedPath.end(), '\\'), droppedPath.end());
    LogF(LOG_TRACE, "+++++++++ ProcessMovie [%s]", droppedPath.c_str());

    // Parse the movie filename and guess the title info (title/date/language/disambiguitor)
    TitleInfo titleInfo = MovieTitle::Guess(OsxUtf8::From(path(droppedPath).filename().string()));
    
    // If the droppedPath is or contains an actual movie, check whether we have subs
    MovieTitle::CheckForSubs(droppedPath, titleInfo);
    
    // Ask allociné first - that way we can use the original title to query imdb
    AlloCineInfo acInfo = AskAlloCine(titleInfo);
    
    // Get movie info from imdb
    TitleInfo imdbQueryInfo;
    imdbQueryInfo.Title((!acInfo.originalTitle.empty())? acInfo.originalTitle: (!acInfo.title.empty())? acInfo.title: titleInfo.Title());
    imdbQueryInfo.Date( (!titleInfo.Date().empty())? titleInfo.Date(): acInfo.year);
    ImdbInfo imdbInfo;// = AskImdb(imdbQueryInfo);
    
    // nothing found -> abort
    if (!acInfo.Found() && !imdbInfo.Found())
    {
        LogF(LOG_TRACE, "Not found:");
        CopyResults(droppedPath);
        return;
    }
    
    // Generate the final info according to all the data we have
    FinalInfo finalInfo = GenerateInfo(titleInfo, acInfo, imdbInfo);

    // Build destination folder name
    string destFolderName = DestinationFolderName(titleInfo, acInfo, imdbInfo, finalInfo);
    path droppedDir=path(droppedPath).parent_path();
    path dstDir=droppedDir/destFolderName; 
    
    // fill the destination folder
    CopyResults(droppedPath, dstDir);
    SetPoster(finalInfo.poster, dstDir);
    OutputInfo(droppedPath, finalInfo, dstDir);
    
    // Categorize the movie:
    // If there is a directory named _Classification one or two levels above the movie,
    // create soft links to the actual movie files in each relevant category folder
    Categorizer::CategorizeMovie(finalInfo, dstDir);
}

static bool IsEnglishNation(const string &nation)
{
    return nation=="U.S.A." || nation=="Grande-Bretagne" || nation=="Irlande" || nation=="Australie";
}

FinalInfo Processor::GenerateInfo(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo)
{
    // does allocine suggest that this is a French version of a foreign movie
    bool preferFrenchInfo = PreferFrenchInfo(titleInfo, acInfo, imdbInfo);
    string firstNationality = acInfo.nationalities.empty()? "": acInfo.nationalities[0];
    
    FinalInfo finalInfo(titleInfo);
    
    // in what cases do we prefer imdb over allocine
    // ----- Life's a beach [FR]
    // allocine returns a movieType="Court-métrage" named "Life's a beach" (134600)
    // imdb find two movies, the first one is the right one from 2010 (tt0285252)
    // since it says [FR]. preferFrenchInfo is true and we pick the wrong allocine hit
    // note that the ac movieType is only visible in the details, not the search
    bool preferImdb=false;
    if (imdbInfo.Found())
        if (!acInfo.Found() || (!preferFrenchInfo && IsEnglishNation(firstNationality)))
            preferImdb=true;
    
    // Choose values
    finalInfo.title = (!acInfo.title.empty())? acInfo.title: imdbInfo.title;
    finalInfo.originalTitle = (!acInfo.originalTitle.empty())? acInfo.originalTitle: (!imdbInfo.title.empty())? imdbInfo.title: acInfo.title;
    finalInfo.languages = titleInfo.GetLanguages();
    if (finalInfo.languages.empty())
    {
        if (preferFrenchInfo)
            finalInfo.languages.AddLanguage(Languages::FR);
        else
            finalInfo.languages.AddLanguage(Languages::VO);
    }
    finalInfo.acId = acInfo.id;
    finalInfo.imdbId = imdbInfo.id;
    finalInfo.year = (!imdbInfo.year.empty() && (preferImdb || acInfo.year.empty()))? imdbInfo.year: acInfo.year;
    finalInfo.rating = (!imdbInfo.rating.empty() && (preferImdb || acInfo.rating.empty()))? imdbInfo.rating: acInfo.rating;
    finalInfo.genres = (!imdbInfo.genres.empty() && (preferImdb || acInfo.genres.empty()))? imdbInfo.genres: acInfo.genres;
    finalInfo.directors = (!imdbInfo.directors.empty() && (preferImdb || acInfo.directors.empty()))? imdbInfo.directors: acInfo.directors;
    finalInfo.cast = (!imdbInfo.cast.empty() && (preferImdb || acInfo.cast.empty()))? imdbInfo.cast: acInfo.cast;
    finalInfo.plot = (!imdbInfo.plot.empty() && (preferImdb || acInfo.plot.empty()))? imdbInfo.plot: acInfo.plot;
    finalInfo.poster = (!imdbInfo.poster.empty() && (preferImdb || acInfo.poster.empty()))? imdbInfo.poster: acInfo.poster;
    
    return finalInfo;
}

string Processor::DestinationFolderName(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo, const FinalInfo &finalInfo)
{
    string firstNationality = acInfo.nationalities.empty()? "": acInfo.nationalities[0];
    bool preferFrenchInfo = PreferFrenchInfo(titleInfo, acInfo, imdbInfo);
    bool preferImdb=false;
    if (imdbInfo.Found())
        if (!acInfo.Found() || (!preferFrenchInfo && IsEnglishNation(firstNationality)))
            preferImdb=true;
    bool preferOriginalTitle = !finalInfo.originalTitle.empty() && !preferFrenchInfo;

    // Build destination folder name
    string destFolderName = preferOriginalTitle? finalInfo.originalTitle: finalInfo.title;
    if (finalInfo.HasSTFR())
    {
        // if we are subtitled, prefer the orignal title
        if (!acInfo.originalTitle.empty() && finalInfo.title != acInfo.originalTitle)
        {
            destFolderName = acInfo.originalTitle+" ["+finalInfo.title+"]";
        }
        else if (!imdbInfo.title.empty() && finalInfo.title != imdbInfo.title)
        {
            destFolderName = imdbInfo.title+" ["+finalInfo.title+"]";
        }
    }
    else if (finalInfo.HasFR())
    {
        if (!imdbInfo.title.empty() && finalInfo.title != imdbInfo.title && firstNationality != "France")
        {
            destFolderName = finalInfo.title+" ["+imdbInfo.title+"]";
        }
    }
    else if (preferImdb)
    {
        destFolderName = imdbInfo.title;
    }    
    
    if (!finalInfo.year.empty())
    {
        destFolderName += " ("+finalInfo.year+")";
    }
    
    if (!finalInfo.languages.empty())
    {
        destFolderName += " [" + string(finalInfo.languages) + "]";
    }

    // replace illegal characters (no : or /)
    destFolderName=ReplaceString(destFolderName, ":", "-");
    destFolderName=ReplaceString(destFolderName, "/", "-");
    
    LogF(LOG_TRACE, "Destination folder name: %s", destFolderName.c_str());
    return destFolderName;
}

// is it a french version of a foreign film
// If it is, we will choose the french title and poster (allocine) instead of the original ones (allocine/imdb)
bool Processor::PreferFrenchInfo(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo)
{
    // if it said so in the filename, trust it
    if (titleInfo.LanguageStr()=="FR") return true;
    
    // if it said so in the filename, trust it
    // VOST sounds like we care about the original version stuff more - not sure but hey
    if (titleInfo.LanguageStr()=="VOSTFR") return false;
    
    // if it said so in the filename, trust it
    if (!titleInfo.LanguageStr().empty()) return false;
    
    // if the filename did not tell us, and we have no allocine match, guess not
    if (acInfo.id.empty()) return false;
    
    // Look at the nationality (Only look at the first nationality)
    string firstNationality = acInfo.nationalities.empty()? "": acInfo.nationalities[0];
    
    // If it's a French movie
    if (firstNationality == "France") return true;
    
    // Look at the orignial title vs. title
    if (acInfo.title != acInfo.originalTitle)
    {
        string lowercaseTitle = to_lower_copy(titleInfo.Title());
        string lowercaseAcTitle = to_lower_copy(acInfo.title);
        string lowercaseAcOTitle = to_lower_copy(acInfo.originalTitle);
        
        int distTitle=Levenshtein::Distance(lowercaseTitle, lowercaseAcTitle);
        int distOTitle=Levenshtein::Distance(lowercaseTitle, lowercaseAcOTitle);
        LogStream(LOG_TRACE) << "Dist: " << distTitle << distOTitle << LogStream::Terminator();
        
        // if the search is closer to the title than to the original, pick French
        if (distTitle<distOTitle)
            return true;
    }
    else
    {
        // If the two titles are the same but the nationality is not USA or England
        // Pick the French version (because we are in France)
        if (!IsEnglishNation(firstNationality))
            return true;
    }
    return false;
}

AlloCineInfo Processor::AskAlloCine(const TitleInfo &titleInfo)
{
    AlloCineInfo acInfo;
    
    try {
        int nbHits = AlloCine::GetId(curl, titleInfo, acInfo, workDir+"/allocineSearch.json");
        if (nbHits==1)
            AlloCine::GrabMovieInfo(curl, acInfo, workDir+"/allocineMovie.json");
        // Special case: if it turns out that it was a short film (Life's a beach (2010)) pretend we did not find anything
        // That's because we cannot get the movieType directly from the search.
        if (acInfo.movieType=="Court-métrage")
            return AlloCineInfo();
        acInfo.nbHits=nbHits;
        return acInfo;
    } catch(std::exception &e) {
        LogF(LOG_ERROR, "AskAlloCine processing failed: %s", e.what());
    } catch(...) {
        LogF(LOG_ERROR, "AskAlloCine processing failed");
    }
    return acInfo;
}

ImdbInfo Processor::AskImdb(const TitleInfo &titleInfo)
{
    ImdbInfo imdbInfo;
    int nbHits = Imdb::GetId(curl, titleInfo, imdbInfo, workDir+"/imdbSearch.json");
    if (nbHits==1)
        Imdb::GrabMovieInfo(curl, imdbInfo, workDir+"/imdbDetails.json");
    imdbInfo.nbHits = nbHits;
    return imdbInfo;
}

void Processor::CopyResults(const path &droppedPath)
{
    path dstDir=droppedPath;
    vector<string> matches;
    if (!is_directory(droppedPath) && ReSplit(droppedPath.string(), "(.*)\\.([a-zA-Z0-9]{3})$", matches))
        dstDir = matches[0];
    CopyResults(droppedPath, dstDir);
}

static void MoveIfExists(const path &file, const path &dstDir)
{
    if (exists(file))
        rename(file, dstDir/file.filename());
}

void Processor::CopyResults(const path &droppedPath, const path &dstDir)
{
    bool isDir=is_directory(droppedPath);
    
    // if the drop was a real folder, just rename it and use it as destination
    if (isDir && exists(droppedPath) && droppedPath!=dstDir)
        rename(droppedPath, dstDir);
    
    // make sure the destination is here
    if (!exists(dstDir)) create_directories(dstDir);
    
    // if the drop was a real file move it into the dest
    if (!isDir && is_regular_file(droppedPath) && droppedPath!=dstDir)
    {
        path moviePath(droppedPath);
        MoveIfExists(moviePath.replace_extension(".sub"), dstDir);
        MoveIfExists(moviePath.replace_extension(".srt"), dstDir);
        MoveIfExists(moviePath.replace_extension(".idx"), dstDir);
        rename(droppedPath, dstDir/droppedPath.filename());
    }
    
    // move every file of the work dir into the result dir
    ofstream logOut((dstDir/"log.txt").c_str());
    fs::directory_iterator end;
    for (fs::directory_iterator itr(workDir); itr!=end; ++itr)
    {
        path crtPath(itr->path());
        if (!exists(crtPath) || is_directory(crtPath))
            continue;

        // Compile work files into log file
        if (crtPath.filename() == "allocineMovie.json"
            || crtPath.filename() == "allocineSearch.json"
            || crtPath.filename() == "imdbDetails.json"
            || crtPath.filename() == "imdbSearch.json"
            || crtPath.filename() == "log.txt")
        {
            ifstream in(crtPath.c_str());
            logOut <<endl<<crtPath.filename()<<endl<<"----------------------"<<endl;
            logOut << in.rdbuf() << endl;
            //remove(crtPath);
        }
        else
        {
            try {
                rename(crtPath, dstDir / crtPath.filename());
            } catch (...) {
                copy_file(crtPath, dstDir / crtPath.filename(), fs::copy_options::overwrite_existing);
                remove (crtPath);
            }
        }
    }
}

void Processor::SetPoster(const string &url, const path &dstDir)
{
    path posterFile=dstDir/"poster.jpg";
    if (!exists(posterFile) && !url.empty())
        curl.GetPayloadFromUrl(url, posterFile.string());
    if (exists(posterFile))
    {
        // set the folder icon
        //string command=string("./seticon \"")+posterFile.string()+"\" \""+dstDir.string() +"\"";
        string command=string("./setFolderIcon.sh \"")+dstDir.string()+"\" \""+posterFile.string() +"\"";
        LogF(LOG_TRACE, "Set Poster: %s", command.c_str());
        std::system(command.c_str());
    }
}

void Processor::OutputInfoXml(const string &droppedPath, const FinalInfo &info, const fs::path &dstDir)
{
    std::ofstream ofs((dstDir/"info.xml").c_str());
    archive::xml_oarchive oa(ofs);
    oa << NVP(info);
}

// write the info file
void Processor::OutputInfo(const string &droppedPath, const FinalInfo &finalInfo, const fs::path &dstDir)
{
    path infoFile=dstDir/"info.txt";
    ofstream out(infoFile.c_str());
    out << "Source         : " << droppedPath << endl;
    out << "Title          : " << finalInfo.title << endl;
    out << "Original Title : " << finalInfo.originalTitle << endl;
    out << "Year           : " << finalInfo.year << endl;
    out << "Director       : " << to_string(finalInfo.directors) << endl;
    out << "Cast           : " << to_string(finalInfo.cast) << endl;
    out << "rating         : " << finalInfo.rating << endl;
    out << "genres         : " << to_string(finalInfo.genres) << endl;
    out << "Language       : " << string(finalInfo.languages) << endl;
    out << "plot           : " << finalInfo.plot << endl;
    out << "Allocine id    : " << finalInfo.acId << endl;
    out << "Imdb id        : " << finalInfo.imdbId << endl;
    
    OutputInfoXml(droppedPath, finalInfo, dstDir);
}


