//
//  say.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 5/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "say.h"
#include <stdlib.h>
using namespace std;

void Say(const std::string &text)
{
    string command("say ");
    command+=text;
    system(command.c_str());
}