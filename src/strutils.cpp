//
//  strutils.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 5/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include "strutils.h"
#include "mylib/log.h"
#include <boost/regex.hpp>
#include <boost/foreach.hpp>
using namespace std;
using namespace boost;
using namespace mylib;

namespace strutils {
    string RemoveString(const std::string &search, const std::string &str)
    {
        string result=str;
        size_t br;
        while ((br = result.find(search)) != string::npos)
            result.erase(br, search.size());
        return result;
    }
    
    string ReplaceString(const string &str, const string &search, const string &replace)
    {
        string result=str;
        size_t br;
        while ((br = result.find(search)) != string::npos)
            result.replace(br, search.size(), replace);
        return result;
    }
    
    // Grab substrings in a string using a RE
    // $1 the input string
    // $2 the regexpr ( it must have () to identify parts to return )
    // $3 the remaining string before
    // $4 the grabbed string
    // $5 the remaining string after
    // Returns success
    bool ReSplit(const string &str, const string &expr, vector<string> &matches)
    {    
        boost::regex expression(expr);
        boost::sregex_iterator m1(str.begin(), str.end(), expression);
        if (m1==boost::sregex_iterator()) return false;
        const boost::sregex_iterator::value_type match(*m1);
        matches.clear();
        for (int i=1; i<match.size(); ++i)
            matches.push_back(match[i]);
        return true;
    }
    
    bool ReMatch(const std::string &str, const std::string &expr)
    {
        boost::sregex_iterator m1(str.begin(), str.end(), boost::regex(expr), boost::match_not_dot_null);
        return m1!=boost::sregex_iterator();
    }
    
    static bool MatchExpr(const string &expression, const string &input, string &output, string &found, string &remaining)
    {
        vector<string> subMatches;
        if (ReSplit(input, expression, subMatches))
        {
            // Johnny English (2003)
            // Sunshine Cleaning[2008]DvDrip[English][Comedy]-FxW
            // -> accept [ as closing separator and pass "English]"
            output = subMatches[0];
            found = subMatches[1];
            remaining = subMatches[2];
            return true;
        }
        return false;
    }
    
    // try a list of clues
    // $1 -> input string
    // $2 <- output string
    // $3 <- found string
    // $4 <- remaining string
    // $5+ -> clues
    // returns
    // 0 -> found
    // 1 <- not found
    bool GrabFromClues(const string &input, string &output, string &found, string &remaining, const vector<string> &clues)
    {
        BOOST_FOREACH(const string &clue, clues)
        {
            // Johnny English (2003)
            // Sunshine Cleaning[2008]DvDrip[English][Comedy]-FxW
            // -> accept [ as closing separator and pass "English]"
            string expression=string("(.*)([[ (._&-]")+clue+"[][ .)_&-])(.*)";
            if (MatchExpr(expression, input, output, found, remaining)) return true;

            expression=string("(.*)([[ (._&-]")+clue+")($)";
            if (MatchExpr(expression, input, output, found, remaining)) return true;

        }
        return false;
    }
    
    // remove leading spaces, trailing spaces and multiple spaces
    // $1 input string
    // $2 output string
    string Strip(const string &input)
    {
        string result = ReplaceString(input, "  ", " ");
        while (!result.empty() && result[0]==' ')
            result = result.substr(1);
        while (!result.empty() && result[result.size()-1]=='-')
            result.resize(result.size()-1);
        while (!result.empty() && result[result.size()-1]==' ')
            result.resize(result.size()-1);
        while (!result.empty() && result[result.size()-1]=='\n')
            result.resize(result.size()-1);
        return result;
    }

    
    // Set $1 to $2 and trace it
    void TraceSet(string &dst, const string &src)
    {
        LogF(LOG_TRACE, "%s [%s]", dst.c_str(), src.c_str());
        dst = src;
    }

}