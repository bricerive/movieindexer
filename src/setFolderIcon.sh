#!/bin/bash
# usage: $0 folder iconFile

folder="$1"
posterIn="$2"
posterTmp=poster_padded.png

convert=/usr/local/bin/convert

# setup temporary images and auto delete upon exit
# use mpc/cache to hold input image temporarily in memory
tmpA="aspect_$$.mpc"
tmpB="aspect_$$.cache"
trap "rm -f $tmpA $tmpB $posterTmp;" 0

# test if posterIn exists and compute dimensions
if $convert -quiet "$posterIn" +repage "$tmpA"
	then
	: 'do nothing'
else
    echo $?
	echo "--- FILE $posterIn DOES NOT EXIST OR IS NOT AN ORDINARY FILE, NOT READABLE OR HAS ZERO SIZE ---"
    exit 1
fi

dwidth0=512
dheight0=512

aspect=`$convert $tmpA -format "%[fx:w/h]" info:`
test0=`$convert xc: -format "%[fx:$aspect>=1?1:0]" info:`
# if portrait mode, swap dwidth and dheight
if [ $test0 -eq 0 ]; then
	aspect=`$convert xc: -format "%[fx:1/$aspect]" info:`
	dwidth=$dheight0
	dheight=$dwidth0
else
	dwidth=$dwidth0
	dheight=$dheight0
fi
test1=`$convert xc: -format "%[fx:$aspect>(${dwidth}/${dheight})?1:0]" info:`
test2=`$convert xc: -format "%[fx:$aspect>=1?1:0]" info:`

if [ $test1 -eq 1 ]; then
	# "aspect larger than desired aspect (wider than tall)"
	resize="${dwidth}x${dwidth}"
elif [ $test2 -eq 1 ]; then
	# "aspect less than desired aspect (wider than tall) but greater than or equal to 1"
	resize="${dheight}x${dheight}^"
else
	# "aspect less than 1 (taller than wide)"
	resize="${dheight}x${dheight}"
fi
$convert \( -size ${dwidth0}x${dheight0} xc:none \) \
	\( $tmpA -filter lanczos -resize $resize \) \
	-gravity center -composite +repage "$posterTmp"


  # !!
  # !! Sadly, Apple decided to remove the `-i` / `--addicon` option from the `sips` utility.
  # !! Therefore, use of *Cocoa* is required, which we do *via Python*, which has the added advantage
  # !! of creating a *set* of icons from the source image, scaling as necessary to create a
  # !! 512 x 512 top resolution icon (whereas sips -i created a single, 128 x 128 icon).
  # !! Thanks, https://apple.stackexchange.com/a/161984/28668
  # !!
  # !! Note: setIcon_forFile_options_() seemingly always indicates True, even with invalid image files, so
  # !!       we attempt no error handling in the Python code.
/usr/bin/python - "$posterTmp" "$folder" <<'EOF' || return
import Cocoa
import sys
Cocoa.NSWorkspace.sharedWorkspace().setIcon_forFile_options_(Cocoa.NSImage.alloc().initWithContentsOfFile_(sys.argv[1].decode('utf-8')), sys.argv[2].decode('utf-8'), 0)
EOF

exit 0
