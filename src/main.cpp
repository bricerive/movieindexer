//
//  main.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 5/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include "categorizer.h"
#include "mylib/config.h"
#include "mylib/configproviderptree.h"
#include "mylib/error.h"
#include "mylib/log.h"
#include "mylib/socket.h"
#include "osxutf8.h"
#include "processor.h"
#include "say.h"
#include "unittest.h"

#include <boost/filesystem.hpp>
// #include <tbb/task_scheduler_init.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace mylib;
using namespace mylib::config;
using namespace std;
using namespace boost;
using namespace boost::filesystem;
namespace fs = boost::filesystem;
// using namespace tbb;

static bool IsMoviesFolder(const path &arg)
{
    return arg.filename() == "Movies";
}

int main(int argc, char *const argv[])
{
    // Get the location for config and log files
    // We use the location of the executable because that is the only thing
    // we can rely on. For example when inside of a DropScript app, the
    // current dir will be /
    // this means that config files must be copied to where the executable
    // is (in the build dir AND in the script dir).
    // Also means that debug runs should start in the build dir
    path mypath(system_complete(path(argv[0])));
    string exeName = mypath.filename().string();
    path exeDir = mypath.parent_path();

    // Setup config
    path configFilePath = exeDir / (exeName + ".config");
    TheProvider::AddProvider(new ProviderXml<Recursive, Attributes>(configFilePath.c_str()));

    // Setup logging
    path logFilePath = exeDir / (exeName + "_log.txt");
    LogFileSink myLog(logFilePath.string());
    CONFIG_PARAM(Log::LogType, "movieIndexer", logLevel);
    Log::SetLevel(logLevel);

    path crtdir(current_path());

    LogF(LOG_TRACE, "################################################");
    LogF(LOG_TRACE, "###        NEW SESSION                       ###");
    LogF(LOG_TRACE, "################################################");
    LogF(LOG_TRACE, "Entering main");
    LogF(LOG_TRACE, "crtdir: %s", crtdir.string().c_str());
    LogF(LOG_TRACE, "mypath: %s", mypath.string().c_str());
    LogF(LOG_TRACE, "Config file: %s", configFilePath.string().c_str());
    LogF(LOG_TRACE, "Log file: %s", logFilePath.string().c_str());

    // init tbb
    // task_scheduler_init init;

    // run unit tests
    // argv[2] indicates the test files location
    if (argc == 3 && !strcmp(argv[1], "test")) {
        UnitTest().Process(argv[2]);
        return 0;
    }

    // reindex mode
    if (argc == 2 && IsMoviesFolder(argv[1])) {
        LogF(LOG_TRACE, "Reclassifying movies folder: [%s]", argv[1]);
        return 0;
    }

    // Process each argument
    for (int i = 1; i < argc; i++) {
        try {
            // Say("Processing argument");
            LogF(LOG_TRACE, "Process argument: [%s]", argv[i]);
            if (strlen(argv[i]) > 4 && !strcmp(argv[i] + strlen(argv[i]) - 4, ".txt"))
                Processor().ProcessList(argv[i]);
            else
                Processor().Process(argv[i]);
        } catch (std::exception &e) {
            LogF(LOG_ERROR, "Item processing failed [%s] %s", argv[i], e.what());
        } catch (...) {
            LogF(LOG_ERROR, "Item processing failed [%s]", argv[i]);
        }
    }
    return 0;
}
