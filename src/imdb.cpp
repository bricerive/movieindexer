//
//  imdb.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "imdb.h"
#include "movietitle.h"
#include "levenshtein.h"
#include "cachedcurl.h"
#include "strutils.h"
#include "mylib/log.h"
#include "mylib/config.h"
#include "mylib/sha1.h"
#include "mylib/time.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace mylib;
using namespace boost;
using namespace strutils;
namespace pt=boost::property_tree;

// so that we have proper codeloc
#define TIM(s1, s2, s3) TraceIfModified(_WHERE, s1, s2, s3)

int Imdb::GetId(CachedCurl &curl, const TitleInfo &titleInfo, ImdbInfo &imdbInfo, const string &jsonPath)
{
    string encoded = curl.Escape(titleInfo.Title());
    
#if 0
    //string url=string("http://app.imdb.com/find?q=") + encoded;

    // used to work:
    //string url=string("anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com/find?q=") + encoded;
    
    
    // from: https://github.com/maddox/imdb-party/commit/2974e620e3213ff98838d4a9b759e417be56f266
//    +      self.class.base_uri 'anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com' if options[:anonymize]
//    +      default_params = {"api" => "v1", "appid" => "iphone1_1", "apiPolicy" => "app1_1", "apiKey" => "2wex6aeu6a8q9e49k7sfvufd6rhh0n", "locale" => "en_US", "timestamp" => Time.now.to_i}
//    +      query_params = default_params.merge(params)
//    +      query_params.each_pair{|key, value| query_param_array << "#{key}=#{URI.escape(value.to_s)}" }
//    +      uri = URI::HTTP.build(:scheme => 'https', :host => host, :path => path, :query => query_param_array.join("&"))
//    +      query_param_array << "sig=app1-#{OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha1'), default_params["apiKey"], uri.to_s)}"
//    +      uri = URI::HTTP.build(:scheme => 'https', :host => host, :path => path, :query => query_param_array.join("&"))
//    +      uri.to_s
//    def find_by_title(title)
//    movie_results = []
//    -      results = self.class.get('/find', :query => {:q => title}).parsed_response
//    +      url = build_url('/find', :q => title)
//    +      results = self.class.get(url).parsed_response
    
    
    //    def find_by_title(title)
    //       url = build_url('/find', :q => title)
    //       results = self.class.get(url).parsed_response
    //    ...
    //    
    //    def build_url(path, params={})
    //       default_params = {"api" => "v1", "appid" => "iphone1_1", "apiPolicy" => "app1_1", "apiKey" => "2wex6aeu6a8q9e49k7sfvufd6rhh0n", "locale" => "en_US", "timestamp" => Time.now.to_i}
    //       query_params = default_params.merge(params)
    //       query_param_array = []
    //       host = self.class.base_uri.gsub('http://', '').to_s
    //       query_params.each_pair{|key, value| query_param_array << "#{key}=#{URI.escape(value.to_s)}" }
    //       query_param_array << "sig=app1-#{OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha1'), default_params["apiKey"], uri.to_s)}"
    //       uri = URI::HTTP.build(:scheme => 'https', :host => host, :path => path, :query => query_param_array.join("&"))
    //       uri.to_s
    //    end
    //    def initialize(options={})
    //       self.class.base_uri 'http://anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com' if options[:anonymize]
    //       self.class.base_uri 'anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com' if options[:anonymize]
    //    end

    CSHA1 sha1;
    string sig = to_string(sha1.Encode("2wex6aeu6a8q9e49k7sfvufd6rhh0n"));
    string staticSig="f67d8b542aa3cc4d1609141199b1f98387635";
    string url=string("anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com/find")+
                      "?api=v1"
                      "&appid=iphone1_1"
                      "&apiPolicy=app1_1"
                      "&apiKey=2wex6aeu6a8q9e49k7sfvufd6rhh0n"
                      "&locale=en_US"
    "&timestamp=" + to_string(static_cast<int>(Time::CurrentTime())) +
                      "&sig=app1-"+ staticSig +
                      "&q=" + encoded;

    //string url=string("http://www.imdbapi.com/?t=") + encoded;
    //http://www.imdbapi.com/?t=True Grit&y=1969
#else
    //http://app.imdb.com/find?api=v1&appid=iphone2_0&device=a1e16eba42d4d1b42a64c4c3bf195d530a1a&locale=en_US&q=Hgggghhghh&timestamp=1304041485&sig=app1_1-f67d8b542aa3cc4d1609141199b1f98387635
    string url = string("anonymouse.org/cgi-bin/anon-www.cgi/http://app.imdb.com/find?api=v1&appid=iphone2_0&device=a1e16eba42d4d1b42a64c4c3bf195d530a1a&locale=en_US&timestamp=1304041485&sig=app1_1-f67d8b542aa3cc4d1609141199b1f98387635&q=")+encoded;
#endif    
    curl.GetPayloadFromUrl(url, jsonPath, "IMDb");
    return Imdb::GetId(titleInfo, imdbInfo, jsonPath);
}

struct Dist
{
    bool filterByDate;
    int date;
    int title;
    Dist(bool filterByDate): filterByDate(filterByDate), date(200), title(200) {}
    bool DateMatch()const {
        CONFIG_PARAM(int, "movieIdexer", imdbDateFuzz);
        return date<=imdbDateFuzz;
    }
    bool ExactDateMatch()const {return date==0;}
    bool ExactTitleMatch()const {return title==0;}
    bool TitleMatch()const {return title<200;}
    bool operator==(const Dist &rhs)const {return date==rhs.date && title==rhs.title;}
    template<typename T> T&operator<<(T&s)const {s<<date<<" "<<title; return s;}

    bool operator<(const Dist &d)const
    {
        if (filterByDate) {
            if (ExactTitleMatch()) {
                if (!d.ExactTitleMatch())
                    return true;
                return date<d.date;
            }
            if (d.ExactTitleMatch())
                return false;
            return date<d.date;
        }                
        return title<d.title;
    }
};
static std::ostream &operator<<(std::ostream &strm, const Dist &d)
{
    if (d.filterByDate)
        strm <<d.date<<"/"<<d.title;
    else
        strm<<d.title;
    return strm;
} //lint !e1929

int Imdb::GetId(const TitleInfo &titleInfo, ImdbInfo &imdbInfo, const string &jsonPath)
{
    LogStream(LOG_TRACE) << "GetId: " << string(titleInfo) << LogStream::Terminator();
    
    // load the JSON DOM
    pt::ptree pt;
    read_json(jsonPath.c_str(), pt);
    
    string lowercaseTitle = to_lower_copy(titleInfo.Title());
    

    string minTConst;
    int minDistCount=0;
    string minTitle;
    
    bool filterByDate = !titleInfo.Date().empty();
    int date = filterByDate? lexical_cast<int>(titleInfo.Date()): 0;
    Dist minDist(filterByDate), startDist(filterByDate);
    
    
    // Loop over the results and pick the best one
    // for GetId: [The Woman in Black: Angels Of Death|2012||], we get one exact title match but with a wrong date of 2014
    // What should win? exact title or exact date? proly title then date.
    // Le Saint -> does not show up until the 20th rank
    pt::ptree results = pt.get_child("data.results", pt::ptree());    
    BOOST_FOREACH(pt::ptree::value_type &v, results)
    {
        string label = v.second.get<string>("label");
        
        
        // Problem: for "neuf mois", the correct result is in "Titles (Exact Matches)", the incorrect one (Nine Months (1995)) is in "Popular Titles"
        // so you don't wanna stop at first hit in "Popular Titles"
        // on the other hand 
        // if (minDistCount==0 && (label=="Titles (Exact Matches)" || label=="Popular Titles" || label=="Titles (Approx Matches)"  || label=="Titles (Partial Matches)"))
        // works for other cases where the popular is better
        if ((label=="Titles (Exact Matches)" || label=="Popular Titles" || label=="Search results")
            || (minDistCount==0 && (label=="Titles (Approx Matches)"  || label=="Titles (Partial Matches)")))

        {
            pt::ptree p = v.second.get_child("list");
            int nbEntries = static_cast<int>(p.size());
            LogStream(LOG_TRACE) << "  nbEntries for [" << label << "]:" << nbEntries << LogStream::Terminator();
            
            BOOST_FOREACH(pt::ptree::value_type &e, p)
            {
                string type = e.second.get<string>("type","");
                //LogStream(LOG_TRACE) << "  type :" << type << LogStream::Terminator();
                
                if (type=="feature" || type=="video" || type=="tv_series")
                {
                    Dist dist(filterByDate);
                    int entryDate = e.second.get<int>("year",0);
                    //LogStream(LOG_TRACE) << "  entryYear : [" <<entryDate<< "] [" <<date<< "]" << LogStream::Terminator();
                    if (filterByDate) dist.date = abs(entryDate-date);
                    string entryTitle = e.second.get<string>("title","");
                    string lowercaseEntryTitle = to_lower_copy(entryTitle);
                    string entryId = e.second.get<string>("tconst");
                    ;
                    dist.title = Levenshtein::Distance(lowercaseTitle, lowercaseEntryTitle);
                    LogStream(LOG_TRACE) << "  entry ["<<entryId<< "|" <<entryTitle<< "|"<<entryDate<<"] dist: ["<<dist<<"]"<<LogStream::Terminator();
                    
                    if (dist<minDist)
                    {
                        minDist = dist;
                        minDistCount = 1;
                        minTConst = e.second.get<string>("tconst");
                        minTitle = entryTitle;
                    }
                    else if (minTitle.empty() && dist==minDist)
                    {
                        minDistCount++;
                    }
                    
                }
            }
            
        }
    }
    
    LogStream(LOG_TRACE) << "  Title hits: " <<minDistCount<< LogStream::Terminator();
    if (minDistCount==1)
    {
        // single hit -> pick that one
        LogStream(LOG_TRACE) << "  Single hit: [" <<minTitle<< "] [" <<minTConst<< "]" << LogStream::Terminator();
        imdbInfo.id=minTConst; 
    }
    return minDistCount;
}

void Imdb::GrabMovieInfo(CachedCurl &curl, ImdbInfo &imdbInfo, const string &jsonPath)
{
    string url=string("http://app.imdb.com/title/maindetails?tconst=")+imdbInfo.id;
    curl.GetPayloadFromUrl(url, jsonPath, "IMDb");
    Imdb::GrabMovieInfo(imdbInfo, jsonPath);
}

static void GetPropertyList(pt::ptree &pt, vector<string> &list)
{
    BOOST_FOREACH(pt::ptree::value_type &v, pt)
    {
        list.push_back(v.second.data());
    }
}


void Imdb::GrabMovieInfo(ImdbInfo &imdbInfo, const string &jsonPath)
{
    LogStream(LOG_TRACE) << "GrabMovieInfo: " << imdbInfo.id << LogStream::Terminator();

    // load the JSON DOM
    pt::ptree pt;
    read_json(jsonPath.c_str(), pt);
    
    BOOST_FOREACH(pt::ptree::value_type &v, pt.get_child("data"))
    {
        if (v.first == "title")
        {
            imdbInfo.title = v.second.data();
            LogStream(LOG_TRACE) << "  title: " << imdbInfo.title << LogStream::Terminator();
        }
        else if (v.first == "year")
        {
            imdbInfo.year = v.second.data();
            LogStream(LOG_TRACE) << "  year: " << imdbInfo.year << LogStream::Terminator();
        }
        else if (v.first == "image")
        {
            imdbInfo.poster = v.second.get<string>("url");
            LogStream(LOG_TRACE) << "  poster: " << imdbInfo.poster << LogStream::Terminator();
        }
        else if (v.first == "rating")
        {
            imdbInfo.rating = v.second.data();
            LogStream(LOG_TRACE) << "  rating: " << imdbInfo.rating << LogStream::Terminator();
        }
        else if (v.first == "genres")
        {
            GetPropertyList(v.second, imdbInfo.genres);
            LogStream(LOG_TRACE) << "  genres: " << to_string(imdbInfo.genres) << LogStream::Terminator();
        }
        else if (v.first == "directors_summary")
        {
            BOOST_FOREACH(pt::ptree::value_type &c, v.second)
            {
                imdbInfo.directors.push_back(c.second.get<string>("name.name", ""));
            }
            LogStream(LOG_TRACE) << "  directors: " << to_string(imdbInfo.directors) << LogStream::Terminator();
        }
        else if (v.first == "cast_summary")
        {
            BOOST_FOREACH(pt::ptree::value_type &c, v.second)
            {
                if (imdbInfo.cast.size()<6) imdbInfo.cast.push_back(c.second.get<string>("name.name", ""));
            }
            LogStream(LOG_TRACE) << "  cast: " << to_string(imdbInfo.cast) << LogStream::Terminator();
        }
        else if (v.first == "plot")
        {
            imdbInfo.plot = v.second.get<string>("outline");
            LogStream(LOG_TRACE) << "  plot: " << imdbInfo.plot.substr(0,50) <<"..."<< LogStream::Terminator();
        }
    }
}

