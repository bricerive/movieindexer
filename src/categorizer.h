#pragma once
//
//  categorizer.h
//  movieIndexer
//
//  Created by Brice Rivé on 9/17/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//
#include <boost/filesystem.hpp>
#include "processor.h"
#include "finalinfo.h"

namespace fs=boost::filesystem;

class Categorizer
{
public:
    static void CategorizeMovies(const string &moviesPath);
    static void CategorizeMovie(const FinalInfo &finalInfo, const fs::path &movieDir);
    static void AddToCategory(const fs::path &categoryDir, const fs::path &movieDir);
private:
    static void LoadInfoFromXml(const fs::path &xmlPath, FinalInfo &info);
};