#pragma once
//
//  cachedcurl.h
//  movieIndexer
//
//  Created by Brice Rivé on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include <string>
#include "curlpp.h"
using namespace std;

class CachedCurl: public CurlPP
{
public:
    CachedCurl(const string &workDir): workDir(workDir) {}
    void GetPayloadFromUrl(const string &url, const string &dumpPath, string checkString="");
    string Get(const string &url);
    void Remove(const string &url);
    bool CheckResult(const string &url, const string &result, const string &validation);
    
private:
    string CachePath(const string &url);

    string workDir;
};