#pragma once
//
//  strutils.h
//  movieIndexer
//
//  Created by Brice Rivé on 5/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include <string>
#include <vector>
using namespace std;

namespace mylib { struct CodeLoc; }

/// just a static container, really
namespace strutils
{
    string RemoveString(const string &search, const string &str);
    string ReplaceString(const string &str, const string &search, const string &replace);
    bool ReSplit(const string &str, const string &expr, vector<string> &matches);
    bool ReMatch(const string &str, const string &expr);
    bool GrabFromClues(const string &input, string &output, string &found, string &remaining, const vector<string> &clues);
    string Strip(const string &input);
    void TraceSet(string &dst, const string &src);
};
