#pragma once
//
//  osxutf8.h
//  movieIndexer
//
//  Created by Brice Rivé on 6/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include <string>
using namespace std;

class OsxUtf8
{
public:
    static string From(const string &str);
    static string To(const string &str);

};