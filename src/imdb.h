#pragma once
//
//  imdb.h
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include <string>
#include <vector>
using namespace std;

class CachedCurl;
struct TitleInfo;

struct ImdbInfo
{
    int nbHits;
    bool Found()const {return nbHits==1;}
    string id;
    string year;
    string poster;
    string rating;
    vector<string> genres;
    vector<string> directors;
    vector<string> cast;
    string plot;
    string title;
    operator string()const
    {
        string str = string("[") +title+ "|" +year+ "|" +id+ "|" +rating+ "]";
        return str;
    }

};

class Imdb
{
public:
    static int GetId(CachedCurl &curl, const TitleInfo &titleInfo, ImdbInfo &imdbInfo, const string &jsonPath);
    static void GrabMovieInfo(CachedCurl &curl, ImdbInfo &imdbInfo, const string &jsonPath);

    static int GetId(const TitleInfo &titleInfo, ImdbInfo &imdbInfo, const string &jsonPath);
    static void GrabMovieInfo(ImdbInfo &imdbInfo, const string &jsonPath);
};