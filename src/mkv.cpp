//
//  mkv.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 05/01/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

// -*- Mode: c++; tab-width: 8; c-basic-offset: 2; indent-tabs-mode: nil -*-
// NOTE: the first line of this file sets up source code indentation rules
// for Emacs; it is also a hint to anyone modifying this file.

// Created   : Sat Nov  6 23:09:59 MDT 2010
// Copyright : Pavel Koshevoy
// License   : MIT -- http://www.opensource.org/licenses/mit-license.php

// yamka includes:
#include "yamka/yamkaElt.h"
#include "yamka/yamkaPayload.h"
#include "yamka/yamkaStdInt.h"
#include "yamka/yamkaFileStorage.h"
#include "yamka/yamkaEBML.h"
#include "yamka/yamkaMatroska.h"

#include "mylib/error.h"
#include <boost/foreach.hpp>

// system includes:
#include <iostream>
#include <iomanip>
#include <string.h>
#include <string>
#include <time.h>

// namespace access:
using namespace Yamka;

//----------------------------------------------------------------
// PartialReader
// 
struct PartialReader : public IDelegateLoad
{
    // virtual:
    uint64 load(FileStorage & storage,
                uint64 payloadBytesToRead,
                uint64 eltId,
                IPayload & payload)
    {
        if (eltId != Segment::TCluster::kId)
        {
            // let the generic load mechanism handle it:
            return 0;
        }
        
        // skip/postpone reading the cluster (to shorten file load time):
        storage.file_.seek(payloadBytesToRead, File::kRelativeToCurrent);
        return payloadBytesToRead;
    }
};

//----------------------------------------------------------------
// Examiner
// 
struct Examiner : public IElementCrawler
{
    Examiner():
    indentation_(0),
    clusterTime_(0)
    {}
    
    void ShowStorageReceipt(IElement & elt)
    {
        IStorage::IReceiptPtr storageReceipt = elt.storageReceipt();
        IStorage::IReceiptPtr payloadReceipt = elt.payloadReceipt();
        
        uint64 eltId = elt.getId();
        IPayload & payload = elt.getPayload();
        
        std::cout
        << indent(indentation_)
        << std::setw(8) << uintEncode(eltId);
        
        std::cout << " -- " << elt.getName();
        
        std::cout << std::endl;
        
        if (eltId == Segment::TCluster::kId)
        {
            Cluster * cluster = dynamic_cast<Cluster *>(&payload);
            clusterTime_ = cluster->timecode_.payload_.get();
        }
    }
    
    void ShowPayload(IElement & elt)
    {
        IPayload & payload = elt.getPayload();
        uint64 eltId = elt.getId();
        const VEltPosition * vEltPos = dynamic_cast<VEltPosition *>(&payload);
        if (vEltPos && !vEltPos->hasPosition()) return;
        
        const VInt * vInt = dynamic_cast<VInt *>(&payload);
        const VUInt * vUInt = dynamic_cast<VUInt *>(&payload);
        const VFloat * vFloat = dynamic_cast<VFloat *>(&payload);
        const VDate * vDate = dynamic_cast<VDate *>(&payload);
        const VString * vString = dynamic_cast<VString *>(&payload);
        const VVoid * vVoid = dynamic_cast<VVoid *>(&payload);
        const VBinary * vBinary = dynamic_cast<VBinary *>(&payload);
        
        std::cout << indent(indentation_);
        
        if (vInt)
        {
            std::cout << "int: " << vInt->get();
        }
        else if (vUInt)
        {
            std::cout << "uint: " << vUInt->get();
        }
        else if (vFloat)
        {
            std::cout << "float: "
            << std::setiosflags(std::ios_base::fixed)
            << vFloat->get();
        }
        else if (vDate)
        {
            struct tm gmt;
            
#ifdef _WIN32
            __time64_t t = __time64_t(kDateMilleniumUTC +
                                      vDate->get() / 1000000000);
            _gmtime64_s(&gmt, &t);
#else
            time_t t = time_t(kDateMilleniumUTC +
                              vDate->get() / 1000000000);
            gmtime_r(&t, &gmt);
#endif
            
            std::cout
            << "date: " << vDate->get() << ", "
            << std::right
            << std::setfill('0') << std::setw(4) << gmt.tm_year + 1900 << '/'
            << std::setfill('0') << std::setw(2) << gmt.tm_mon + 1 << '/'
            << std::setfill('0') << std::setw(2) << gmt.tm_mday << ' '
            << std::setfill('0') << std::setw(2) << gmt.tm_hour << ':'
            << std::setfill('0') << std::setw(2) << gmt.tm_min << ':'
            << std::setfill('0') << std::setw(2) << gmt.tm_sec;
        }
        else if (vString)
        {
            std::cout << "string: " << vString->get();
        }
        else if (vVoid)
        {
            std::cout << "void, size " << vVoid->get();
        }
        else if (vBinary)
        {
            if (eltId == BlockGroup::TBlock::kId ||
                eltId == Cluster::TSimpleBlock::kId ||
                eltId == Cluster::TEncryptedBlock::kId)
            {
                SimpleBlock block;
                block.importData(vBinary->data_);
                
                short int offset = block.getRelativeTimecode();
                std::cout << "track "
                << std::right << std::setfill(' ') << std::setw(3)
                << block.getTrackNumber()
                << ", abs time "
                << std::right << std::setfill(' ') << std::setw(10)
                << (clusterTime_ + offset);
                
                if (block.isKeyframe())
                {
                    std::cout << ", key";
                }
                
                if (block.isDiscardable())
                {
                    std::cout << ", discardable";
                }
                
                std::cout << ", " << block.getNumberOfFrames()
                << " frames";
            }
            else
            {
                std::cout << "variable size binary data";
                uint64 binSize = vBinary->data_.numBytes();
                if (binSize)
                {
                    std::cout << ", size " << binSize;
                }
            }
        }
        else if (vEltPos)
        {
            std::cout << "element position";
            
            const IElement * elt = vEltPos->getElt();
            if (!elt)
            {
                std::cout << ", unresolved";
            }
            else if (elt->storageReceipt())
            {
                IStorage::IReceiptPtr storageReceipt = elt->storageReceipt();
                std::cout
                << ", resolved to "
                << elt->getName()
                << "(" << uintEncode(elt->getId()) << ")";
                
            }
        }
        else
        {
            IStorage::IReceiptPtr payloadReceipt = elt.payloadReceipt();
            std::cout << "binary data, size " << payload.calcSize();
            
            if (payloadReceipt)
            {
                TByteVec data;
                if (Yamka::load(payloadReceipt, data) && data.size())
                {
                    std::cout << ", ";
                    std::cout << data;
                }
            }
        }
        
        if (!elt.mustSave())
        {
            std::cout << ", default value";
        }
        
        std::cout << std::endl;
        
    }
    
    // virtual:
    bool eval(IElement & elt)
    {
        IStorage::IReceiptPtr storageReceipt = elt.storageReceipt();
        IStorage::IReceiptPtr payloadReceipt = elt.payloadReceipt();
        
        IPayload & payload = elt.getPayload();
        
        if (storageReceipt)
        {
            ShowStorageReceipt(elt);
        }
        
        Indent::More indentMore(indentation_);
        
        if (payload.isComposite())
        {
            EbmlMaster * ebmlMaster = dynamic_cast<EbmlMaster *>(&payload);
            BOOST_FOREACH(IPayload::TVoid & eltVoid, ebmlMaster->voids_)
            {
                eval(eltVoid);                
            }            
            payload.eval(*this);
        }
        else if (payloadReceipt)
        {
            ShowPayload(elt);
        }
        
        return false;
    }
    
    unsigned int indentation_;
    uint64 clusterTime_;
};


//----------------------------------------------------------------
// Examiner
// 
struct TrackCollector : public IElementCrawler
{
    unsigned int GetPayloadUInt(IElement & elt)
    {
        IPayload & payload = elt.getPayload();
        const VUInt * vUInt = dynamic_cast<VUInt *>(&payload);
        if (!vUInt) throw mylib::Err(_WHERE, "payload type mismatch");
        return vUInt->get();
    }
    
    std::string GetPayloadString(IElement & elt)
    {
        IPayload & payload = elt.getPayload();
        const VString * vString = dynamic_cast<VString *>(&payload);
        if (!vString) throw mylib::Err(_WHERE, "payload type mismatch");
        return vString->get();
    }
    
    // virtual:
    bool eval(IElement & elt)
    {
        IStorage::IReceiptPtr storageReceipt = elt.storageReceipt();
        IStorage::IReceiptPtr payloadReceipt = elt.payloadReceipt();
        
        IPayload & payload = elt.getPayload();
        
        if (payload.isComposite())
        {
            // recurse down composite nodes
            EbmlMaster * ebmlMaster = dynamic_cast<EbmlMaster *>(&payload);
            BOOST_FOREACH(IPayload::TVoid & eltVoid, ebmlMaster->voids_)
            {
                eval(eltVoid);                
            }            
            payload.eval(*this);
        }
        else if (storageReceipt && payloadReceipt)
        {
            // handle the nodes we care about
            switch (elt.getId())
            {
                case 0x83: // Track Type - uint
                    trackType = GetPayloadUInt(elt);
                break;
                case 0x22B59C: // Language
                {
                    std::string language = GetPayloadString(elt);
                    switch (trackType)
                    {
                        case 2: // Audio
                            if (language=="fra" || language=="fre")
                                fr=true;
                            else
                                vo=true;
                            break;
                        case 17: // Subs
                            if (language=="fra" || language=="fre")
                                stfr=true;
                            else
                                stvo=true;
                            break;
                    }
                    trackType=0;
                }  break;
            }
        }
        
        return false;
    }
    
    TrackCollector(bool &fr, bool &vo, bool &stfr, bool &stvo)
    : fr(fr), vo(vo), stfr(stfr), stvo(stvo)
    {
        fr=vo=stfr=stvo=false;
    }
    
    bool &fr, &vo, &stfr, &stvo;
    unsigned int trackType;
};

void HasTracks(const std::string &path, bool &fr, bool &vo, bool &stfr, bool &stvo)
{
    FileStorage src(path, File::kReadOnly);
    if (!src.file_.isOpen())
        throw mylib::Err(_WHERE, "failed to open %s for reading",path.c_str());
    
    uint64 srcSize = src.file_.size();
    MatroskaDoc doc;
    
    PartialReader fastLoader;
    uint64 bytesRead = 0;
    
    if (doc.loadSeekHead(src, srcSize) && doc.loadViaSeekHead(src, &fastLoader, false))
    {
        bytesRead = srcSize;
    }
    
    if (!bytesRead || doc.segments_.empty())
        throw mylib::Err(_WHERE, "source file [%s] has no matroska segments",path.c_str());
    
//    Examiner examiner;
//    doc.eval(examiner);
    TrackCollector trackCollector(fr,vo,stfr,stvo);
    doc.eval(trackCollector);
    // If we saw a ubtitile with no language, assume STVO
    if (trackCollector.trackType==17) stvo=true;
    
    // close open file handles:
//    doc = MatroskaDoc();
//    src = FileStorage();
}

