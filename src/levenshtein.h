#pragma once
//
//  levenshtein.h
//  movieIndexer
//
//  Created by Brice Rivé on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
// Include STL string type

#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
using namespace std;
namespace pt=boost::property_tree;

namespace Levenshtein {

    int Distance(const string source, const string target);

}