//
//  osxutf8.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 6/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "osxutf8.h"
#include "mylib/error.h"
#include <iconv.h>
#include <vector>
using namespace mylib;

// Convert from normal UTF-8 to OSX UTF-8
// $1 -> fileName
// $2 <- converted
string OsxUtf8::To(const string &str)
{
    iconv_t cd = iconv_open("UTF8-MAC", "UTF-8");
    vector<char> inTmp(str.begin(), str.end());
    vector<char> outTmp(str.size()*2); // Utf8 is longer than UTF8-MAC
    char *inbuf = &inTmp[0];
    char *outbuf = &outTmp[0];
    size_t inbytesleft = inTmp.size();
    size_t outbytesleft = outTmp.size()*2;
    iconv(cd, &inbuf, &inbytesleft, &outbuf, &outbytesleft);
    if (inbytesleft!=0)
        throw Err(_WHERE, "incomplete iconv call");
    iconv_close(cd);
    return string(&outTmp[0]);
    
}


// Unicode allows some accented characters to be represented in several different ways:
// as a "code point" representing the accented character, or as a series of code points
// representing the unaccented version of the character, followed by the accent(s).
// For example, "ä" could be represented either precomposed as U+00E4 (UTF-8 0xc3a4,
// Latin small letter 1 with diaeresis) or decomposed as U+0061 U+0308 (UTF-8 0x61cc88,
// Latin small letter a + combining diaeresis).
//
// OS X's HFS+ filesystem requires that all filenames be stored in the UTF-8 representation
// of their fully decomposed form. In an HFS+ filename, "ä" MUST be encoded as 0x61cc88,
// and "ö" MUST be encoded as 0x6fcc88.
//
// So what's happening here is that your shell script contains "Böhmáí" in precomposed
// form, so it gets stored that way in the variable a, and stored that way in the
// text file. But when you create a file with that name (with touch), the filesystem
// converts it to the decomposed form for the actual filename. And when you ls it, it
// shows the form the filesystem has: the decomposed form.
string OsxUtf8::From(const string &str)
{
    // shortcut
    if (str.find('\314')==string::npos)
        return str;
    
    // ISO-8859-1 is also known as "Latin 1"
    // OSX uses utf-8 but imdb uses ISO-8859-1
    // Mädchen, Mädchen (2001)
    // L'armée du crime (2009)
    //
    //TraceIfModified titleInfo.title "`echo titleInfo.title | iconv -f UTF8-MAC -t ISO-8859-1`" "Utf8 to Iso-8859-1 -> " fileName	
    //
    // Hide non-ascci characters to avoid problems with all the string processing
    // replace unicode non-ascii with $x$
    // $1 -> input string
    // $2 <- output string
    // Asciify()
    // {
    // 	replaced=`echo "$1" | iconv --unicode-subst='\u%u'  -f UTF8-MAC -t ASCII`
    //	eval $2=\"$replaced\"
    // }
    //
    //Asciify "$droppedFile" asciified
    //Trace "Asciified: [$asciified]"
    //TraceIfModified "$droppedFile" "$asciified" "Replace non Ascii:" droppedFile
    //
    //droppedFile=`echo "$droppedFile" | iconv -f UTF8-MAC -t ISO-8859-1`
    //TraceIfModified "$droppedFile" "`echo "$droppedFile" | iconv -f UTF8-MAC -t ISO-8859-1`" "Utf8 to Iso-8859-1 -> " droppedFile	
    // return to UTF-8
    //TraceIfModified titleInfo.title "`echo titleInfo.title  | iconv -f ISO-8859-1 -t UTF8-MAC`" "Utf8 to Iso-8859-1 -> " fileName	
    //
    // Convert from OSX UTF-8 to normal UTF-8
    // $1 -> fileName
    // $2 <- converted
    
    //echo $1 | hexdump -b -c
    
    // é : 145,314,201
    //	local c=`printf "%b%b%b" "\145" "\314" "\201"`
    //	local escaped=`echo "$escaped" | sed -e 's/'$c'/é/g'`
    
    // è : 145,314,201
    //	local c=`printf "%b%b%b" "\145" "\314" "\200"`
    //	local escaped=`echo "$escaped" | sed -e 's/'$c'/è/g'`
    
    // î : 151,314,202
    //	local c=`printf "%b%b%b" "\151" "\314" "\202"`
    //	local escaped=`echo "$escaped" | sed -e 's/'$c'/î/g'`
    
    // ç : 151,314,202
    //	local c=`printf "%b%b%b" "\143" "\314" "\247"`
    //	local escaped=`echo "$escaped" | sed -e 's/'$c'/ç/g'`
    
    iconv_t cd = iconv_open("UTF-8", "UTF8-MAC");
    vector<char> inTmp(str.begin(), str.end());
    vector<char> outTmp(str.size()); // we can use the same size since Utf8 is shorter than UTF8-MAC
    char *inbuf = &inTmp[0];
    char *outbuf = &outTmp[0];
    size_t inbytesleft = inTmp.size();
    size_t outbytesleft = outTmp.size();
    iconv(cd, &inbuf, &inbytesleft, &outbuf, &outbytesleft);
    if (inbytesleft!=0)
        throw Err(_WHERE, "incomplete iconv call");
    iconv_close(cd);
    return string(&outTmp[0]);
}
