//
//  cachedcurl.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "cachedcurl.h"
#include "strutils.h"
#include "mylib/log.h"
#include <boost/filesystem.hpp>
#include <fstream>
#include <streambuf>

using namespace strutils;
using namespace mylib;
using namespace boost;
using boost::filesystem::path;

void CachedCurl::GetPayloadFromUrl(const string &url, const string &dumpPath, string checkString)
{
    string http=Get(url);
    unsigned long htmlHeaderEndPos = http.find("\r\n\r\n");
    if (htmlHeaderEndPos==string::npos) throw Err(_WHERE, "Cannot find http header end mark");
    string payload = http.substr(htmlHeaderEndPos+4);
    ofstream out(dumpPath.c_str(), ios::binary);
    out << payload;
    if (!checkString.empty() && !CheckResult(url, http, checkString)) throw Err(_WHERE, "Cannot find url get check string");
}


// $1 -> Url
// $2 <- path to cache entry
string CachedCurl::CachePath(const string &url)
{
    string curlCache = workDir + "/Cache";
    string escapedUrl = ReplaceString(url, ":/", "_");
    string cccpCachedFileName = curlCache + "/" + escapedUrl;
    return cccpCachedFileName;
}

bool IsReadable(path &file)
{
    ifstream i(file.c_str());
    return bool(i);
}

// $1 -> Url
// result on stdout -> No trace here!!!
// When the script is run directly from TextMate (with Cmd-R),
// Hands Off! might block it. -> Disable Hands Off!
string CachedCurl::Get(const string &url)
{
	path cachedFileName(CachePath(url));
	path cachedFileDir = cachedFileName.parent_path();
    if (!is_directory(cachedFileDir))
        create_directories(cachedFileDir);
    
    // if the cache exists, check that it is correct
    if (exists(cachedFileName))
    {
        
        if (!is_regular_file(cachedFileName)  || !IsReadable(cachedFileName))
        {
            LogStream(LOG_STATUS) << "CachedCurl: bad chache file ... delete" << cachedFileName << LogStream::Terminator();
            remove(cachedFileName);
        }
    }
	
    string page;
    if (exists(cachedFileName))
    {
        // If the cache exists, use it
        ifstream in(cachedFileName.c_str());
        page = string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
    }
    else
    {
        // if there is no cache, fetch with curl
        string curlOutput= workDir+"/curlOutput.txt";
        GetUrl(url, page);
        ofstream out(cachedFileName.c_str(), ios::binary);
        out << page;
    }
    return page;
}

// $1 -> Url
// remove the cache entry for this URL
void CachedCurl::Remove(const string &url)
{
    path cachedFileName(CachePath(url));
    remove(cachedFileName);
}

// $1 -> Url
// $2 -> curl result
// $3 -> string it must contain to be valid
bool CachedCurl::CheckResult(const string &url, const string &result, const string &validation)
{
    if (result.empty())
    {
        LogStream(LOG_STATUS) << "CachedCurl: curl failed (empty)" << LogStream::Terminator();
        return false;
    }
    if (result.find(validation) == string::npos)
    {
        LogStream(LOG_STATUS) << "CachedCurl: curl failed["<<url<<"], missing: " << validation << LogStream::Terminator();
        return false;
    }
    return true;
}
