#pragma once
//
//  movietitle.h
//  movieIndexer
//
//  Created by Brice Rivé on 6/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/split_member.hpp>
using namespace std;
namespace fs=boost::filesystem;
#define NVP BOOST_SERIALIZATION_NVP

struct Languages
{
    Languages() :language(NONE) {}

    enum { NONE=0x00, FR=0x01, VO=0x02, STFR=0x04, STVO=0x08};
    typedef unsigned long LanguageFlags;
    
    void AddLanguage(LanguageFlags val) {language|=val;}
    bool HasLanguage(LanguageFlags val)const {return language&val;}

    void SetFromString(const std::string &val) {
        language = (val=="FR")? FR: (val=="VOSTFR")? STFR: (val=="VO")? VO: (val=="VOSTEN")? STVO: NONE;
    }
    operator string()const;
    bool empty()const {return language==NONE;}

    /// support boost serialization
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        string s(*this);
        ar << boost::serialization::make_nvp("value", s);
    }
    
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
        string s;
        ar >> boost::serialization::make_nvp("value", s);
        SetFromString(s);
    }

private:
    LanguageFlags language;
};

struct TitleInfo
{
    void Title(const std::string &val) {title=val;}
    const std::string &Title()const {return title;}
    void Date(const std::string &val) {date=val;}
    const std::string &Date()const {return date;}
    void Disambiguator(const std::string &val) {disambiguator=val;}
    const std::string &Disambiguator()const {return disambiguator;}
    
    void LanguageFromString(const std::string &val) {languages.SetFromString(val);}
    std::string LanguageStr()const {return string(languages);}
    const char *LanguageCStr()const {return string(languages).c_str();}
    void AddLanguage(Languages::LanguageFlags val) {languages.AddLanguage(val);}
    const Languages &GetLanguages()const {return languages;}

    operator string()const
    {
        string str = string("[") + title + "|" + date  + "|" + string(languages) + "|" + disambiguator + "]";
        return str;
    }
        
private:
    string title;
    string date;
    string disambiguator;
    string orignalTitle;
    Languages languages;
};


class MovieTitle
{
public:
    static TitleInfo Guess(const std::string &inputFileName);
    static void CheckForSubs(const std::string &droppedPath, TitleInfo &titleInfo);

private:
    static bool FindLanguage(const string &fileName, const vector<string> &clues, Languages::LanguageFlags language, TitleInfo &titleInfo);
    static void CheckForSubFiles(const fs::path &moviePath, const vector<string> &clues, Languages::LanguageFlags language, TitleInfo &titleInfo);
};