#pragma once
//
//  unittest.h
//  movieIndexer
//
//  Created by Brice Rivé on 6/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include "cachedcurl.h"
#include "mylib/log.h"
#include <string>
using namespace std;

class UnitTest
{
public:
    UnitTest();
    bool Process(const char *testFilesPath);
private:
    bool AllocineGrab_ut(const string &line);
    bool GuessMovieTitle_ut(const string &line);
    bool ImdbGrab_ut(const string &line);
    void PushTrace(mylib::Log::LogType logLevel);
    void PopTrace();
    typedef bool (UnitTest::*TestFnc)(const string &line);
    struct Test {
        const char *name;
        TestFnc method;
    };
    bool TestEachLine(const char *testFilesPath, const Test &test);

    string workDir;
    CachedCurl curl;
    mylib::Log::LogType level;
};
