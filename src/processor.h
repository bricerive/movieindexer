#pragma once
//
//  processor.h
//  movieIndexer
//
//  Created by Brice Rivé on 5/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#include "cachedcurl.h"
#include "movietitle.h"
#include "finalinfo.h"
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <sstream>
namespace fs=boost::filesystem;
namespace pt=boost::property_tree;
using namespace std;
#define NVP BOOST_SERIALIZATION_NVP

namespace mylib { struct CodeLoc; }

struct TitleInfo;
struct AlloCineInfo;
struct ImdbInfo;

class Processor
{
public:
    Processor();
    void Process(const string &droppedPath);
    void ProcessList(const string &path);


private:
    void ProcessWrapped(const string &droppedPath);
    bool PreferFrenchInfo(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo);
    AlloCineInfo AskAlloCine(const TitleInfo &titleInfo);
    ImdbInfo AskImdb(const TitleInfo &titleInfo);
    void CopyResults(const fs::path &moviePath);
    void CopyResults(const fs::path &moviePath, const fs::path &dstName);
    string DestinationFolderName(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo, const FinalInfo &finalInfo);
    void SetPoster(const string &url, const fs::path &dstDir);
    void OutputInfo(const string &droppedPath, const FinalInfo &finalInfo, const fs::path &dstDir);
    void OutputInfoXml(const string &droppedPath, const FinalInfo &finalInfo, const fs::path &dstDir);
    FinalInfo GenerateInfo(const TitleInfo &titleInfo, const AlloCineInfo &acInfo, const ImdbInfo &imdbInfo);

    string workDir;
    CachedCurl curl;
};