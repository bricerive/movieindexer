//
//  movietitle.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 6/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "movietitle.h"
#include "mkv.h"
#include "mylib/log.h"
#include "strutils.h"
#include <boost/assign.hpp>
#include <boost/foreach.hpp>
using namespace std;
using namespace mylib;
using namespace strutils;
using namespace boost::assign;

// so that we have proper codeloc
#define TIM(ti, s2, s3) TraceIfModified(_WHERE, ti, s2, s3)

// compare $1 value with $2
// if they are different, output a trace with $3 as message and set $1 to $2
static void TraceIfModified(const mylib::CodeLoc &codeLoc, TitleInfo &titleInfo, const string &val, const string &msg)
{
    if (val==titleInfo.Title()) return;
    LogStream(Log::trace, codeLoc) << msg << " : [" << titleInfo.Title() << "] -> [" << val << "]" << LogStream::Terminator();
    titleInfo.Title(val);
}

// replace a string by another if the second is shorter
// $1 <-> name of the first variable
// $2 <- value to compare and update to
static void UpdateIfShorter(TitleInfo &titleInfo, const string &val)
{
    if (val.size() < titleInfo.Title().size())
        titleInfo.Title(val);
}

// Guess a movie title from a movie filename
// $1 -> filename (in UTF-8 , non OSX)
// $2 <- title
// $3 <- date
// $4 <- language (VO / FR / VOSTFR)
// $5 <- disambiguator
TitleInfo MovieTitle::Guess(const std::string &inputFileName)
{
    TitleInfo titleInfo;
    
    string fileName = inputFileName;
    
    // rename input
    LogF(LOG_TRACE, "GuessMovieTitle: [%s]", fileName.c_str());
    titleInfo.Title(fileName);
    
    // DropScript will convert / into : so we need to convert back
    TIM(titleInfo, ReplaceString(titleInfo.Title(), ":", "/"), "replace [:] by [/]");
    
    // grab the extension
    string remain, extensionValue;
    vector<string> subStrings;
    if (ReSplit(titleInfo.Title(), "(.*)\\.([a-zA-Z0-9]{3})$", subStrings))
    {
        // 	La Vie sexuelle des Belges 1950-1978.avi
        fileName=subStrings[0];
        titleInfo.Title(fileName);
        LogF(LOG_TRACE, "  extension: %s -> [%s]", subStrings[1].c_str(), fileName.c_str());
    }
    
    // Look for an imdb disambiguator
    // can also contain category information:
    if (ReSplit(fileName, "([^(]*)\\(([12][90][0-9][0-9])(/[IV]*)\\).*", subStrings))
    {
        // 	Armageddon (1998/I)
        UpdateIfShorter(titleInfo, subStrings[0]);
        titleInfo.Date(subStrings[1]);
        titleInfo.Disambiguator(subStrings[1]+subStrings[2]);
        LogF(LOG_TRACE, "  disambiguator: %s [%s] -> [%s]", titleInfo.Date().c_str(), titleInfo.Disambiguator().c_str(), titleInfo.Title().c_str());
    }
    else if (ReSplit(fileName, "([^(]*)\\(([IV]*)\\) \\(([12][90][0-9][0-9])\\).*", subStrings))
    {
        // 	Volver (I) (2006)
        UpdateIfShorter(titleInfo, subStrings[0]);
        titleInfo.Date(subStrings[2]);
        titleInfo.Disambiguator(subStrings[2]+"/"+subStrings[1]);
        LogF(LOG_TRACE, "  disambiguator: %s [%s] -> [%s]", titleInfo.Date().c_str(), titleInfo.Disambiguator().c_str(), titleInfo.Title().c_str());
    }
    else if (ReSplit(fileName, "([^(]*)\\(([^12]*) ([12][90][0-9][0-9])\\).*", subStrings))
    {
        // 	Aeon Flux (TV Series 1991)
        // 	Todo Sobre Mi Madre (Almodovar -1999) (Vostfr).avi
        UpdateIfShorter(titleInfo, subStrings[0]);
        titleInfo.Date(subStrings[2]);
        titleInfo.Disambiguator(subStrings[1]+" "+subStrings[2]);
        LogF(LOG_TRACE, "  disambiguator: %s [%s] -> [%s]", titleInfo.Date().c_str(), titleInfo.Disambiguator().c_str(), titleInfo.Title().c_str());
    }
    else if (ReSplit(fileName, "([^(]*)\\(([12][90][0-9][0-9])\\) \\(V\\).*", subStrings))
    {
        // 	Next Avengers- Heroes Of Tomorrow (2008) (V)
        // 	-> Next Avengers: Heroes of Tomorrow (Video 2008)
        UpdateIfShorter(titleInfo, subStrings[0]);
        titleInfo.Date(subStrings[1]);
        titleInfo.Disambiguator(string("Video ")+titleInfo.Date());
        LogF(LOG_TRACE, "  disambiguator: %s [%s] -> [%s]", titleInfo.Date().c_str(), titleInfo.Disambiguator().c_str(), titleInfo.Title().c_str());
    }
    else if (ReSplit(fileName, "([^(]*)\\(([12][90][0-9][0-9])\\) \\(TV\\).*", subStrings))
    {
        // 	Lightspeed (2006) (TV) [Stan Lee's - DVDRip Xvid fasamoo LKRG]
        // 	-> Lightspeed (TV 2006)
        UpdateIfShorter(titleInfo, subStrings[0]);
        titleInfo.Date(subStrings[1]);
        titleInfo.Disambiguator(string("TV ")+titleInfo.Date());
        LogF(LOG_TRACE, "  disambiguator: %s [%s] -> [%s]", titleInfo.Date().c_str(), titleInfo.Disambiguator().c_str(), titleInfo.Title().c_str());
    }
    else
    {
        // Find the date if it's in the filename
        // note: I used to accept the date without enclosings but it failed on movies like "Class 1984"
        if (ReSplit(fileName, "(.*)\\(([12][90][0-9][0-9])[^)]*\\)", subStrings))
        {
            // 	Defiance (2008) [FrSub]
            // 	Johnny English (2003)
            // 	Fragile(s) (2007).avi
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  Prenthesized date: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "([^[]*)\\[([12][90][0-9][0-9])[^]]*\\]", subStrings))
        {
            // [1984 -vo]
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  Bracketed date: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "([^(]*)\\([^12]*([12][90][0-9][0-9])[^)]*\\)", subStrings))
        {
            // 	Todo Sobre Mi Madre (Almodovar -1999) (Vostfr).avi
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  Prenthesized date2: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "(.*)[[(._&-]([12][90][0-9][0-9])[].)_&-]", subStrings))
        {
            // 	Little.Nicky.DVDRip.Xvid.2000-tots
            // 	Blues Brothers 2000
            // 	Class 1984 [VOSTFR].avi
            // 	La Vie sexuelle des Belges 1950-1978.avi
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  Bracketed date2: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "(.*) - ([12][90][0-9][0-9])", subStrings))
        {
            // 	Comme les 5 Doigts de la Main - 2010 [DVDRIP].avi
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  dashed date after: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "([12][90][0-9][0-9]) - (.*)", subStrings))
        {
            // 1936 - Pépé le Moko
            UpdateIfShorter(titleInfo, subStrings[1]);
            titleInfo.Date(subStrings[0]);
            LogF(LOG_TRACE, "  dashed date before: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
        else if (ReSplit(fileName, "(.*)\\.([12][90][0-9][0-9])", subStrings))
        {
            // Legitime.Defense.2011
            UpdateIfShorter(titleInfo, subStrings[0]);
            titleInfo.Date(subStrings[1]);
            LogF(LOG_TRACE, "  date after dot: %s -> [%s]", titleInfo.Date().c_str(), titleInfo.Title().c_str());
        }
    }
    if (titleInfo.Disambiguator().empty())
        titleInfo.Disambiguator(titleInfo.Date());
    
    
    // find the language
    vector<string> frClues = {"TRUEFRENCH","FRENCH","FR","French","french","Fr","- VF","fr","vf"};
    FindLanguage(fileName, frClues, Languages::FR, titleInfo);

    vector<string> stfrClues =  list_of("VO\\+STFR")("VostFr")("VOSTFr")("VOSTFR")("VOST")("Vostfr")("Vost")("vost")("Vo-S")("vostfr")("FrSub")("Vostf")("VOSTF")("VoStFr")("VosTfr")("VOStFr");
    FindLanguage(fileName, stfrClues, Languages::STFR+Languages::VO, titleInfo);

    vector<string> stvoClues = list_of("EnSub")("EngSub")("VOSTEN")("VOSTEn");
    FindLanguage(fileName, stvoClues, Languages::STVO+Languages::VO, titleInfo);
    
    // Johnny English (2003)
    // Sunshine Cleaning[2008]DvDrip[English][Comedy]-FxW
    vector<string> voClues = list_of("VO")("ENG")("Eng")("English]");
    FindLanguage(fileName, voClues, Languages::VO, titleInfo);
    
    // remove bracketed comments
    // [Rec] 2  -> only if it does not start with '['
    if (ReSplit(fileName, "([^[]+)\\[([^]]*)", subStrings))
    {
        UpdateIfShorter(titleInfo, subStrings[0]);
        LogF(LOG_TRACE, "  Bracketed comment: %s -> [%s]", subStrings[1].c_str(), titleInfo.Title().c_str());
    }
    
    // remove parenthesized comments
    // 	LOL (Laughing Out Loud) ® (2008).avi:LOL (Laughing Out Loud):2008::2008
    // 	Fragile(s) (2007).avi:Fragile(s):2007::2007
    // 	Bliss (whip it):Bliss (whip it):::
    //if (ReSplit(fileName, "([^(]*)\\(([^)]*)\\)(.*)", subStrings))
    //{
    //    UpdateIfShorter(titleInfo, subStrings[0]);
    //    LogF(LOG_TRACE, "  Parenthesized comment: %s -> [%s]", subStrings[1].c_str(), titleInfo.Title().c_str());
    //}
    
    // remove keyword comments
    vector<string> commentKeyWords=list_of("BDRip")("BDrip")("DvDRip")("DVDRIP")("XVID")("DVDrip")("DVDRip")("DvDRip")("DVDRiP")("DvDrip")("DvdRip")("dvdrip")
    ("Xvid")("KLAXXON")("-aXXo")("UNRATED")("patlefourbe")("_by_char")("LIMITED")("LiMiTED")("READNFo")("by_miloute")("720p")("-tc")("cd1")("cd2")("CD1")("CD2");
    BOOST_FOREACH(string commentKeyWord, commentKeyWords)
    {
        string expression = string("(.*)(") + commentKeyWord + ".*)(.*)";
        if (ReSplit(fileName, expression, subStrings))
        {
            UpdateIfShorter(titleInfo, subStrings[0]);
            LogF(LOG_TRACE, "  Keyword comment: %s -> [%s]", subStrings[1].c_str(), titleInfo.Title().c_str());
        }
    }
    
    // from here down, we transform the shortest gmTitle we found so far
    
    // remove spaces
    string stripped = Strip(titleInfo.Title());
    TIM(titleInfo, stripped, "  stripped");
    
    // remove periods
    // the idea here is that, long filenames with no space might have dots instead of spaces
    // 	G.I. Joe
    // 	R.T.T. (2009).avi
    //  J.Aime.Regarder.Les.Filles.2011.FRENCH.DVDRip.XviD-UTT.avi
    // de.force.2011.french.bdrip.xvid-tickets.avi
    // only do it if it's longer than some
    if (titleInfo.Title().size()>6)
    {
        // if there is no space in the middle
        if (!ReMatch(titleInfo.Title(), "[^ ]* [^ ]*"))
        {
            // 	The.Karate.Kid.2010.PROPER.FRENCH.BDRiP.XViD-THENiGHTMARE
            // 	The.Chronicles.of.Narnia.The.Voyage.of.the.Dawn.Treader.avi
            // 	Mr. Deeds (2002)
            // 	G.I. Joe
            // 	Et si c'était vrai...
            // 	Tron.french.by.aligator427.(1982).avi
            string tmp = ReplaceString(titleInfo.Title(), ".", " ");
            TIM(titleInfo, tmp, "  Periods removal");
        }
        // if it does not end in ...
        if (!ReMatch(titleInfo.Title(), "\\.\\.\\.$"))
        {
            string tmp = titleInfo.Title();
            if (tmp[tmp.size()-1]=='.')
            {
                tmp.resize(tmp.size()-1);
            }
            TIM(titleInfo, tmp, "  End period removal");
        }
    }
    
    // remove spaces
    stripped = Strip(titleInfo.Title());
    TIM(titleInfo, stripped, "  Stripped");
    
    // remove dashed space replacement
    // 	Tonnerre-sous-les-tropiques-tc.French.avi
    if (!ReMatch(titleInfo.Title(), "[^ ]* [^ ]*"))
    {
        string tmp = ReplaceString(titleInfo.Title(), "-", " ");
        TIM(titleInfo, tmp, "  Dash removal");
    }
    
    // remove spaces
    stripped = Strip(titleInfo.Title());
    TIM(titleInfo, stripped, "  Stripped");
    
    // remove underscore space replacement
    // 	Le_Tombeau
    if (!ReMatch(titleInfo.Title(), "[^ ]* [^ ]*"))
    {
        string tmp = ReplaceString(titleInfo.Title(), "_", " ");
        TIM(titleInfo, tmp, "  Underscore removal");
    }
    
    // remove spaces
    stripped = Strip(titleInfo.Title());
    TIM(titleInfo, stripped, "  Stripped");

    LogStream(LOG_TRACE) << "  -> " << string(titleInfo) << LogStream::Terminator();
    return titleInfo;
}

bool MovieTitle::FindLanguage(const string &fileName, const vector<string> &clues, Languages::LanguageFlags language, TitleInfo &titleInfo)
{
    string output, keyword, remaining;
    if (GrabFromClues(fileName, output, keyword, remaining, clues))
    {
        titleInfo.AddLanguage(language);
        UpdateIfShorter(titleInfo, output);
        LogF(LOG_TRACE, "  Language %s: %s -> [%s]", titleInfo.LanguageCStr(), keyword.c_str(), titleInfo.Title().c_str());
        return true;
    }
    return false;
}

void MovieTitle::CheckForSubs(const std::string &droppedPath, TitleInfo &titleInfo)
{
    if (!fs::exists(droppedPath)) return;

    // look for a movie file
    fs::path moviePath;
    const unsigned long movieBytesMin=100000000;
    if (fs::is_regular_file(droppedPath))
    {
        unsigned long nBytes = fs::file_size(droppedPath);
        if (nBytes >= movieBytesMin)
            moviePath = droppedPath;
    }
    else if (fs::is_directory(droppedPath))
    {
        // look in there to see if there is a movie file
		fs::directory_iterator endIter;
		for (fs::directory_iterator dirIter(droppedPath) ; dirIter!=endIter; ++dirIter ) {
            if (fs::is_regular_file(*dirIter))
            {
                unsigned long nBytes = fs::file_size(*dirIter);
                if (nBytes >= movieBytesMin)
                {
                    moviePath = *dirIter;
                    break;
                }
            }
		}
    }
    if (moviePath.empty()) return;
    
    // Look for subtitle files
    vector<string> vostfrClues = list_of("-FR")(".FR")("-Fra")(".fre")("");
    CheckForSubFiles(moviePath, vostfrClues, Languages::STFR+Languages::VO, titleInfo);
    vector<string> vostClues = list_of("-EN")("-en")(".en")(".English")(".Eng")("-Eng");
    CheckForSubFiles(moviePath, vostClues, Languages::STVO+Languages::VO, titleInfo);

    // Is it a mkv file
    if (moviePath.extension()==".mkv")
    {
        bool fr, vo, stfr, stvo;
        HasTracks(moviePath.string(), fr, vo, stfr, stvo);
        if (fr) titleInfo.AddLanguage(Languages::FR);
        if (vo) titleInfo.AddLanguage(Languages::VO);
        if (stfr) titleInfo.AddLanguage(Languages::STFR);
        if (stvo) titleInfo.AddLanguage(Languages::STVO);
    }
}

void MovieTitle::CheckForSubFiles(const fs::path &moviePath, const vector<string> &clues, Languages::LanguageFlags language, TitleInfo &titleInfo)
{
    BOOST_FOREACH(const string &clue, clues)
    {
        fs::path subPath = (moviePath.parent_path()/moviePath.stem()).string()+clue+".sub";
        if (fs::exists(subPath) && fs::is_regular_file(subPath))
        {
            // we have subs
            titleInfo.AddLanguage(language);
        }
        // is there a .srt file with the same name
        subPath.replace_extension(".srt");
        if (fs::exists(subPath) && fs::is_regular_file(subPath))
        {
            // we have subs
            titleInfo.AddLanguage(language);
        }
    }
}


Languages::operator string()const
{
    string str;
    struct { LanguageFlags id; const char *s; } toString[] = { {FR,"FR"}, {VO,"VO"}, {STFR,"STFR"}, {STVO,"STVO"} };
    const int nbToStrings = sizeof(toString)/sizeof(toString[0]);
    
    for (int i=0; i<nbToStrings; i++)
    {
        if (language & toString[i].id)
        {
            if (!str.empty())
                str += "+";
            str += toString[i].s;
        }
    }
    
    return str;
}
