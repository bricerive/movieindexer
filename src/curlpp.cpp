/*
 *  CurlPP.cpp
 *  DockTicker
 *
 *  Created by Brice Rive on Mon Dec 15 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */

#include "CurlPP.h"
#include "StrUtils.h"
#include "mylib/log.h"
#include "mylib/from_string.h"
#include <iostream> // for cout
#include <curl/curl.h>
#include <boost/regex.hpp>

#define WANTS_DUMP 0
#define VERBOSE 0
#define DUMP WANTS_DUMP && _DEBUG

using std::cout;
using std::string;
using namespace mylib;
using namespace std;

// Error strings
const string CurlPP::curlErrorWhy("Curl error");
const string CurlPP::inaccessibleServerErrorWhy("Cannot access server");
const string CurlPP::internalServerErrorWhy("HTTP status = 500 (Internal Server Error)");
const string CurlPP::unhandledHttpStatusErrorWhy("unexpected HTTP status");

// Safari's signature
//static const string defaultUserAgent = "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us) AppleWebKit/103u (KHTML, like Gecko) Safari/100.1";
// IE Mac static const string defaultUserAgent = "Mozilla/4.0 (compatible; MSIE 5.22; Mac_PowerPC)";
// IE PC
//static const string defaultUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 1.0.3705)");
static const string defaultUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:8.0.1) Gecko/20100101 Firefox/8.0.1");

extern "C" {
	// Function pointer that should match the following prototype:
	// size_t function( void *ptr, size_t size, size_t nmemb, void *stream);
	// This function gets called by libcurl as soon as there is data reveiced
	// that needs to be saved. The size of the data pointed to by ptr is size
	// multiplied with nmemb, it will not be zero terminated. Return the number
	// of bytes actually taken care of. If that amount differs from the amount
	// passed to your function, it'll signal an error to the library and it will
	// abort the transfer and return CURLE_WRITE_ERROR.
	//
	// Set the stream argument with the CURLOPT_WRITEDATA option.
	//
	// NOTE: you will be passed as much data as possible in all invokes, but
	// you cannot possibly make any assumptions. It may be one byte, it may be
	// thousands. The maximum amount of data that can be passed to the write
	// callback is defined in the curl.h header file: CURL_MAX_WRITE_SIZE.
	static size_t writeFunction(void *buffer, size_t size, size_t nmemb, void *userp)
    {
		string *str = reinterpret_cast<string *>(userp);
		str->append(reinterpret_cast<char *>(buffer), size*nmemb);
		return size*nmemb;
    }

	// Function pointer that should match the following prototype:
	//  size_t function( void *ptr, size_t size, size_t nmemb, void *stream);.
	// This function gets called by libcurl as soon as there is received header
	// data that needs to be written down. The headers are guaranteed to be written
	// one-by-one and only complete lines are written. Parsing headers should be
	// easy enough using this. The size of the data pointed to by ptr is size
	// multiplied with nmemb. The pointer named stream will be the one you passed
	// to libcurl with theCURLOPT_WRITEHEADER option. Return the number of bytes
	// actually written or return -1 to signal error to the library (it will cause
	// it to abort the transfer with a CURLE_WRITE_ERROR return code).
	size_t headerFunction( void *ptr, size_t size, size_t nmemb, void *stream)
    {
		string headerLine;
		for (unsigned int i=0; i<size*nmemb; i++)
			headerLine += reinterpret_cast<char *>(ptr)[i];
		reinterpret_cast<CurlPP *>(stream)->ParseHeaderLine(headerLine);
		return size*nmemb;
	}
}

CurlPP::CurlPP()
{
    easyhandle = curl_easy_init();

#if VERBOSE    
    curl_easy_setopt(easyhandle, CURLOPT_VERBOSE, 1);
#endif
	
    // set the error message buffer
    curl_easy_setopt(easyhandle, CURLOPT_ERRORBUFFER, curlErrorBuffer);
    
    // set the data receive handler
    curl_easy_setopt(easyhandle, CURLOPT_WRITEFUNCTION, writeFunction);
    
    // We need the headers
    curl_easy_setopt(easyhandle, CURLOPT_HEADER, true);
    curl_easy_setopt(easyhandle, CURLOPT_HEADERFUNCTION, headerFunction);
    curl_easy_setopt(easyhandle, CURLOPT_WRITEHEADER, this);
    
    // Set a decent agent as it is needed pretty much everywhere on the Web
    curl_easy_setopt(easyhandle, CURLOPT_USERAGENT, defaultUserAgent.c_str());
	
	// Ignore signal for multithreaded apps
    curl_easy_setopt(easyhandle, CURLOPT_NOSIGNAL, true);
}

void CurlPP::SetAgent(const std::string &agent)
{
    curl_easy_setopt(easyhandle, CURLOPT_USERAGENT, agent.c_str());
}

CurlPP::~CurlPP()
{
	curl_easy_cleanup(easyhandle);
}

string CurlPP::HostPart(const string &url)
{
    unsigned long hostBeg = url.find("//");
	unsigned long hostEnd = url.find("/", hostBeg+2);
    return url.substr(0, hostEnd);
}

unsigned long CurlPP::Perform(const string &url, string &content)
{
    CURLcode success;
	content="";
	crtHeaders.clear();
    curl_easy_setopt(easyhandle, CURLOPT_FILE, &content);
	curl_easy_setopt(easyhandle, CURLOPT_URL, url.c_str());
	curl_easy_setopt(easyhandle, CURLOPT_REFERER, currentUrl.c_str());
    
    // disable CA check directly here
    // - because it's baqsed on using a file in the Curl stage directoyry that isonly accessible on build machine
    // - because we don;t care for it
	curl_easy_setopt(easyhandle, CURLOPT_SSL_VERIFYPEER, false);
    
	if ((success = curl_easy_perform(easyhandle)) != CURLE_OK) {
		LogF(LOG_ERROR, "CURL error: %s", curlErrorBuffer);
		throw Err(_WHERE, curlErrorWhy);
	}
	curl_easy_setopt(easyhandle, CURLOPT_HTTPGET, true); // default
	unsigned long httpStatus;
	if ((success =curl_easy_getinfo(easyhandle, CURLINFO_RESPONSE_CODE, &httpStatus)) != CURLE_OK) {
		LogF(LOG_ERROR, "CURL error: %s", curlErrorBuffer);
		throw Err(_WHERE, curlErrorWhy);
	}
	return httpStatus;
}

string RelativeToAbsoluteUrl(const string &relative, const string &current)
{
//	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//	ObjCAutoRelease killer(pool);
//	NSURL *mergedURL = [NSURL URLWithString: StrConv(relative) relativeToURL: [NSURL URLWithString:StrConv(current)]];
//	return StrConv([mergedURL absoluteString]);
    if (relative.find("http://")==string::npos)
        return current + "/" + relative;
    return relative;
}

string HostOfUrl(const string &url)
{
//	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//	ObjCAutoRelease killer(pool);
//	NSURL *nsUrl = [NSURL URLWithString: StrConv(url)];
//	return StrConv([nsUrl host]);
    unsigned long pos = url.find("http://");
    if (pos == string::npos) throw Err(_WHERE, "bad url");
    string host = url.substr(7);
    pos = host.find("/");
    if (pos == string::npos) throw Err(_WHERE, "bad url");
    host.resize(pos);
    return host;
}

string GrabString(const string &content, const string &tagB, const string &tagE)
{
    size_t tagBpos=0, tagEpos=content.size();
	if (tagB.size()>0) {
		tagBpos = content.find(tagB);
		if (tagBpos==string::npos)
			throw Err(_WHERE, "Beginning tag not found");
	}
    unsigned long valPos = tagBpos + tagB.size();
	if (tagE.size()>0) {
		tagEpos = content.find(tagE, valPos);
		if (tagEpos==string::npos)
			throw Err(_WHERE, "End tag not found");
	}
    return content.substr(valPos, tagEpos-valPos);
}

// look for cookie declarations and concatenate them in result string
string GrabCookies(const string &header)
{
    string result;
    const string cookieTag = "Set-Cookie: ";
    const string cookieTag2 = "Set-cookie: ";
    size_t cookiePos=0, cookieStart=0, cookieEnd, cookieEol, cookieEqual;
    
    // the headers below are valid, so we need to parse more precisely
    //  Set-Cookie: HttpOnly
    //  Set-Cookie: HttpOnly=;path=;domain=;
    while ((cookiePos = header.find(cookieTag, cookieStart)) != string::npos
           ||(cookiePos = header.find(cookieTag2, cookieStart)) != string::npos) {
        cookieStart = cookiePos + cookieTag.size();
        cookieEol = header.find('\n', cookieStart); // to eol
        cookieEqual = header.find('=', cookieStart);
        cookieEnd = header.find(";", cookiePos);
        if (cookieStart)
            result += "; ";
        if (cookieEqual>=cookieEol)
        {
            result += header.substr(cookieStart, cookieEol-1-cookieStart)+"=";
        }
        else if (cookieEnd>=cookieEol)
        {
            result += header.substr(cookieStart, cookieEol-1-cookieStart);            
        }
        else
        {
            result += header.substr(cookieStart, cookieEnd-cookieStart);            
        }
        cookiePos = cookieEol;
    }
    
    return result;
}

// replacement for deprecated SCNetworkCheckReachabilityByName
#include <SystemConfiguration/SCNetworkReachability.h>
static Boolean NetworkCheckReachabilityByName(const char *nodename,SCNetworkConnectionFlags *flags)
{
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithName(NULL, nodename);
    Boolean ok = SCNetworkReachabilityGetFlags(target, flags);
    CFRelease(target);
    return ok;
}

/*bool IsAccessibleHost(const string &hostName)
{
	SCNetworkConnectionFlags flags;
	if (!NetworkCheckReachabilityByName(hostName.c_str(), &flags)) return false;
	if ((flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired)) return true;
	return false;
}*/

void CurlPP::GetUrl(const string &url, string &page)
{
    bool received=false;
    string content, headerVal;
	currentUrl = RelativeToAbsoluteUrl(url, currentUrl);
	//string hostName = HostOfUrl(currentUrl);
	//if (!IsAccessibleHost(hostName)) throw Err(_WHERE, inaccessibleServerErrorWhy);
#if DUMP
	unsigned long startOfServerName = currentUrl.find("//")+2;
	std::string serverNameAndRestOfUrl = currentUrl.substr(startOfServerName);
	std::string serverName = serverNameAndRestOfUrl.substr(0, serverNameAndRestOfUrl.find("/")) +".txt";
	std::ofstream file(serverName.c_str(), std::ios::out|std::ios::app);
#endif

    while (!received) {
		unsigned long httpStatus = Perform(currentUrl, content);
		AddMetaHttpHeaders(content);
#if DUMP
		file << "#########################################" << std::endl;
		file << "### URL: " << currentUrl << std::endl;
		file << "### Cookies: " << cookies << std::endl;
		file << "### Content: " << content << std::endl;
#endif
        switch (httpStatus) {
            case 100: // Continue -> do it again
                break;
            case 200: // OK -> done
				if (HasHeader("refresh", headerVal)) {
					string seconds = GrabString(headerVal, "", ";");
					string refresh;
					try {
						refresh = GrabString(headerVal, "URL=", "");
					} catch (Err &err) {
						refresh = GrabString(headerVal, "url=", "");
					}
					sleep(from_string<int>(seconds)); 
					currentUrl = RelativeToAbsoluteUrl(refresh, currentUrl);
				} else
					received=true;
				break;
			case 302: // Redirect
			case 301: // Moved permanently
				if (HasHeader("location", headerVal)) {
					currentUrl = RelativeToAbsoluteUrl(headerVal, currentUrl);
				}
				break;
            case 304: // Refresh -> do it again
                break;
			case 500: // 500 Internal Server Error: The server encountered an unexpected condition which prevented it from fulfilling the request.
                throw Err(_WHERE, internalServerErrorWhy);
            default:
				LogF(LOG_ERROR, "Unhandled HTTP status = %d [%s]", httpStatus, url.c_str());
                throw Err(_WHERE, unhandledHttpStatusErrorWhy);
        }
        MergeCookies(GrabCookies(content)); // The headers are inside of content too!
    }
    page = content;
}

void CurlPP::PostUrl(const string &url, const string &post, string &page)
{
	//LogF(LOG_STATUS, url + " <- " + post);
    curl_easy_setopt(easyhandle, CURLOPT_POSTFIELDS, post.c_str());
    GetUrl(url, page);
}

// HTTP header syntax:
// <name>: <value>
void CurlPP::ParseHeaderLine(const string &headerLine)
{
	unsigned int i;
	string name, value;
	for (i=0; i<headerLine.size();) {
		char c = headerLine[i++];
		if (c==':') break;
		name += tolower(c);
	}
	for (; i<headerLine.size(); ++i) {
		char c = headerLine[i];
		if (c!=' ' && c!='\t') break;
	}
	for (; i<headerLine.size(); ++i) {
		char c = headerLine[i];
		if (c=='\r' && headerLine[i+1]=='\n') break;
		value += c;
	}
	crtHeaders.push_back(make_pair(name, value));
}

class AddAHeader {
public:
	AddAHeader(CurlPP::Headers & curlHdrs_):curlHdrs(curlHdrs_) {}
	bool operator()(const boost::smatch &what);
private:
	CurlPP::Headers &curlHdrs;
};

bool AddAHeader::operator()(const boost::smatch &what) 
{
	curlHdrs.push_back(make_pair(what[1].str(), what[2].str()));
	return true;
}

// Http header equivalents located in the html itself!!!
// <head>
// <meta http-equiv="CacheControl" content = "no-cache" />
// <meta http-equiv="Pragma" content = "no-cache" />
// <meta http-equiv="Expires" content = "-1" />
// <meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../../../g1_e/ssl/identification/3-acc_ide1.ide"/>
// <title>Cr&eacute;dit Agricole - Services de banque en ligne</title>
// </head>
void CurlPP::AddMetaHttpHeaders(const std::string &content)
{
	const std::string expr("<[[:space:]]*[mM][eE][tT][aA][[:space:]]*[hH][tT][tT][pP]-[eE][qQ][uU][iI][vV][[:space:]]*=[[:space:]]*\"(.*?)\"[[:space:]]*[cC][oO][nN][tT][eE][nN][tT][[:space:]]*=[[:space:]]*\"(.*?)\"");
	boost::regex expression(expr);
	boost::sregex_iterator m1(content.begin(), content.end(), expression);
	boost::sregex_iterator m2;
    std::for_each(m1, m2, AddAHeader(crtHeaders));
}

// case-independent (ci) equality binary function
// for use by ciStringEqual
template <class T>
struct ci_equal_to : public std::binary_function<T,T,bool> 
{
	bool operator() (const T& c1, const T& c2) const 
    { return tolower (c1) == tolower (c2); }
};

// case-independent (ci) string compare
// returns true if strings are EQUAL
bool ciStringEqual (string s1, string s2)
{
	
	std::pair <string::const_iterator, 
	string::const_iterator> result =
    mismatch (s1.begin (), s1.end (),   // source range
              s2.begin (),              // comparison start
              ci_equal_to<unsigned char> ());  // comparison
	
	// match if both at end
	return result.first == s1.end () &&
		result.second == s2.end ();
	
} // end of ciStringEqual 

// Needs to be case-independent
bool CurlPP::HasHeader(const string &name, string &value)
{
	for (Headers::iterator i=crtHeaders.begin(); i!=crtHeaders.end(); i++) {
		if (ciStringEqual(i->first,name)) {
			value = i->second;
			return true;
		}
	}
	return false;
}

void CurlPP::ResetCookies()
{
	cookies.clear();
	curl_easy_setopt(easyhandle, CURLOPT_COOKIE, cookies.c_str());
}

void MergeCookies(string &cookies, const string &newCookies)
{
    // parse new cookies
    size_t nameB, nameE, valB, valE, crt=0;
    while ((nameE=newCookies.find("=", crt))!=string::npos) {
        nameB=crt;
        nameE=nameE-1;
        valB=nameE+2;
        valE = newCookies.find(";", valB);
        if (valE==string::npos)
            valE = newCookies.size()-1;
        else
            valE=valE-1;
        string name = newCookies.substr(nameB, nameE-nameB+1);
        string val = newCookies.substr(valB, valE-valB+1);
        if ((nameB=cookies.find(name)) != string::npos) {
            size_t oldValB=cookies.find("=",nameB)+1;
            size_t oldValE = cookies.find(";", oldValB);
            if (oldValE==string::npos)
                oldValE = cookies.size()-1;
            else
                oldValE=oldValE-1;
            cookies.erase(oldValB,oldValE-oldValB+1);
            cookies.insert(oldValB, val);
        } else {
            if (cookies.size()==0)
                cookies = name + "=" + val;
            else
                cookies = cookies + "; " + name + "=" + val;
        }
        crt = valE+3;
    }
}

void CurlPP::MergeCookies(const std::string &newCookies)
{
    ::MergeCookies(cookies, newCookies);
    curl_easy_setopt(easyhandle, CURLOPT_COOKIE, cookies.c_str());
}

void CurlPP::DisableCertificates()
{
	curl_easy_setopt(easyhandle, CURLOPT_SSL_VERIFYPEER, false);
}

void CurlPP::SetSslVersion(int version)
{
	curl_easy_setopt(easyhandle, CURLOPT_SSLVERSION, version);
}

// Encode a title for seach
// $1 -> title
// $2 <- encoded
string CurlPP::Escape(const string &input)
{
    char *escaped = curl_easy_escape(easyhandle, input.c_str(), 0); 
    string result(escaped);
    curl_free(escaped);
    return result;
}


