//
//  finalinfo.cpp
//  movieIndexer
//
//  Created by Brice Rivé on 9/17/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#include "finalinfo.h"

FinalInfo::FinalInfo(const TitleInfo &titleInfo)
{
    title = titleInfo.Title();
    languages = titleInfo.GetLanguages();
    year = titleInfo.Date();
}
