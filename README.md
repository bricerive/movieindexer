# About movieindexer and the project #

### What is movieindexer for? ###

* Automated indexing of movie files for OSX
    * Identify movies from file names
    * Get movie information
    * Install movie into it's own folder with a nice icon
    * Add movie to a set of classifications
* Version 0.0.0

### How do I get set up? ###

* How to build
    * Grab the depot
    * Run CMake - You need these libraries
        * [MyLib}(https://bitbucket.org/bricerive/mylib)
        * boost::system
        * boost::filesystem
        * boost::regex
        * boost::serialization
        * ssl
        * crypto
        * curl
        * z
        * ebml
        * matroska
        * iconv
    * Build with XCode
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* The test module uses test data files that are not under the repo.


### Who do I talk to? ###

* Brice Rivé
